
var to_address_array = [];
var cc_address_array = [];
var send_to_address_array = [];
var send_cc_address_array = [];
var json_save_address_book;
var json_save_address_search;
var json_hme_users;

$.fn.modal.Constructor.prototype.enforceFocus = function() {
     modal_this = this
     $(document).on('focusin.modal', function (e) {
         // Fix for CKEditor + Bootstrap IE issue with dropdowns on the toolbar
         // Adding additional condition '$(e.target.parentNode).hasClass('cke_contents cke_reset')' to
         // avoid setting focus back on the modal window.
         if (modal_this.$element[0] !== e.target && !modal_this.$element.has(e.target).length
             && $(e.target.parentNode).hasClass('cke_contents cke_reset')) {
             modal_this.$element.focus()
         }
     })
 };
 
$(document).ready(function() {

	$('#rte_modal').draggable({
		handle: "#email_header, .email-footer-buttons"
	});
	
	$('#email_contacts').draggable({
		handle: "#email_contacts, #email_contact_header"
	});
	
	var editorElement = CKEDITOR.document.getById( 'modal_rte_textarea' );

	CKEDITOR.replace( 'modal_rte_textarea', {
	    customConfig: '/js/ckeditor/editorConfig.js'
	});

	
	$('.modal-content').resizable({
		//alsoResize: ".modal-dialog",
		minHeight: 300,
		minWidth: 800,
		handles: 'e, w'
	});
	
	
	jQuery.each(jQuery('textarea[data-autoresize]'), function() {
		var offset = this.offsetHeight - this.clientHeight;
		var resizeTextarea = function(el) {
		   jQuery(el).css('height', 'auto').css('height', el.scrollHeight + offset);
		};
		jQuery(this).on('keyup input paste propertychange change', function() {resizeTextarea(this); }).removeAttr('data-autoresize');
	});
	
	
	$('.attachment_add_remove').mouseover(function(e){
		this.style.cursor='pointer';
	});
	
	$('#send_attachments').click(function(e){
		$(this).blur();		
		//if($(this).val() == '') return;
		var count = $("#email_ul_attachments_list").children().length;
		//console.log(count);
		if(count <= 0) return;
		$('#email_attachments_list').toggle('slow', function() {
		});	
	});
	
	$('#email_attachments_list').mouseleave(function(e){
		//if (e===undefined) e= window.event;
		//var target= 'target' in e? e.target : e.srcElement;
		//if (target!==this){return;}	
		$(this).toggle('slow');	
	});
	
	$('.attachment_add_remove').click(function(e){
		var attachments = '';
		if($(this).hasClass('fa-check')){
			$(this).removeClass('fa-check attachment-attach').addClass('fa-times attachment-unattach');
		}else{
			$(this).removeClass('fa-times attachment-unattach').addClass('fa-check attachment-attach');
		}
		
		$('.attachment_add_remove.attachment-attach').each(function() {
			attachments += (attachments == '' ? '' : '; ') + $(this).parents('li').children('span').html();
		});	
		$('#send_attachments').val(attachments).change();
	});
	
	$('#address_book_cancel_btn').click(function(e){
		$('#email_contacts').hide();
	});
	
	$('#address_book_ok_btn').click(function(e){
		var to_addr = $('#send_to_address').val();
		var cc_addr = $('#send_cc_address').val();
		to_addr = to_addr.replace(/\s/g,'').replace(/,/g,';').replace(/;/g,'; ')
		cc_addr = cc_addr.replace(/\s/g,'').replace(/,/g,';').replace(/;/g,'; ')
		var select_to_addr = $('#email_to_selected').val();
		var select_cc_addr =  $('#email_cc_selected').val();
		select_to_addr = select_to_addr.replace(/\s/g,'').replace(/,/g,';').replace(/;/g,'; ')
		select_cc_addr = select_cc_addr.replace(/\s/g,'').replace(/,/g,';').replace(/;/g,'; ')
		var addr_list = '';
		if($('#email_to_selected').val() != ''){
			addr_list = (to_addr == '' ? select_to_addr : '; ' + select_to_addr);
			$('#send_to_address').val(to_addr + addr_list).change();
		}
		if(select_cc_addr != ''){
			addr_list = (cc_addr == '' ? select_cc_addr : '; ' + select_cc_addr);
			$('#send_cc_address').val(cc_addr + addr_list).change();
		}
		$('#email_to_selected').val('').change();
		$('#email_cc_selected').val('').change();
		$('#email_contacts').hide();
	});
	
	$('#get_to_address').click(function(e){	
		var obj = $('#email_to_selected');
		var email_list = '';
		to_address_array = obj.val().replace(/\s/g,'').replace(/,/g,';').split(';')
		send_to_address_array = $('#send_to_address').val().replace(/\s/g,'').replace(/,/g,';').split(';');
		send_cc_address_array = $('#send_cc_address').val().replace(/\s/g,'').replace(/,/g,';').split(';');
		$('#email_contact_book .user-selected-color').each(function(i, obj) {
			//console.log($(obj).prop("classList"));
			if($(obj).hasClass('user-selected-color')){
				if(jQuery.inArray($(obj).attr('email'), to_address_array) < 0 
					&& jQuery.inArray($(obj).attr('email'), cc_address_array) < 0
					&& jQuery.inArray($(obj).attr('email'), send_to_address_array) < 0
					&& jQuery.inArray($(obj).attr('email'), send_cc_address_array) < 0
				)
					to_address_array.push($(obj).attr('email'));
				$(obj).removeClass('user-selected-color');
			}
		});
		//$('.tr-emp').removeClass('user-selected-color');
		obj.val('');
		$.each(to_address_array, function(index, item) {
			if(item != '')
				email_list += (email_list == '' ? item : '; ' + item);
		});	
		obj.val(email_list).change();	
	});
	
	$('#get_cc_address').click(function(e){	
		var obj = $('#email_cc_selected');
		var email_list = '';
		cc_address_array = obj.val().replace(/\s/g,'').replace(/,/g,';').split(';')
		$('#email_contact_book .user-selected-color').each(function(i, obj) {
			if($(obj).hasClass('user-selected-color')){
				if(jQuery.inArray($(obj).attr('email'), to_address_array) < 0 
					&& jQuery.inArray($(obj).attr('email'), cc_address_array) < 0
					&& jQuery.inArray($(obj).attr('email'), send_to_address_array) < 0
					&& jQuery.inArray($(obj).attr('email'), send_cc_address_array) < 0
				)
					cc_address_array.push($(obj).attr('email'));
				$(obj).removeClass('user-selected-color');
			}
		});
		obj.val('');
		$.each(cc_address_array, function(index, item) {
			if(item != '')
				email_list += (email_list == '' ? item : '; ' + item);
		});	
		obj.val(email_list).change();	
	});	

	$('.search-request-types').hide();
	
	$('#search_address_book').keyup(function(e){
		setTimeout(function(){
			if($('#search_address_book').val() != ''){
				autocomplete($('#search_address_book').val());
			}
		}, 10);
		
	});
	
	$('.search-type').click(function(e){
		switch($(this).val()){
			case 'address_book' :
				$('#email_contact_book').html('');
				$('#email_contact_book').append(json_save_address_book);
				set_contact_properties();	
				$('#search_address_book').prop('disabled',true)
				$('.search-request-types').hide();
				break;
			case 'address_search' :
				$('#email_contact_book').html('');
				$('#search_address_book').val('');
				$('#search_address_book').prop('disabled',false)
				$('.search-request-types').show();
				break;
		}
	});	

	$('.search-request').click(function(e){
		if($('#search_address_book').val() != ''){
				autocomplete($('#search_address_book').val());
		}
	});
	
	////build_address_book(json_hme_users, 'givenname');	
	////json_save_address_book = contacts;	
});

function set_contact_properties(){
	$('.emp-td').click(function(e){	
		toggle_emp_select($(this));
	});	
	$('.tr-emp').mouseover(function(e){	
		if(!$(this).hasClass('user-selected-color'))
			$(this).addClass('mouseover');
		this.style.cursor = 'pointer';
	
	});	
	$('.tr-emp').mouseout(function(e){	
		$(this).removeClass('mouseover');
	});
}

function showAddress_book(email_contacts){
	$('#email_to_selected').val('').change();
	$('#email_cc_selected').val('').change();
	$('#email_contacts').show();
}


function send_message(){
//CKEDITOR.instances.modal_rte_textarea.setData('<p style="margin-left:0in; margin-right:0in"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:16.5pt"><span style="font-family:&quot;Arial&quot;,sans-serif"><span style="color:#3670b1">&larr; all postsxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx</span></span></span></span></span></p>');

	if($('.email-send-button').hasClass('email_send_btn_active')) return;
	var editorElement = CKEDITOR.document.getById( 'modal_rte_textarea' );
	json_email_resp.emailcontent = CKEDITOR.instances.modal_rte_textarea.getData();
	json_email_resp.to = $('#send_to_address').val();
	json_email_resp.cc = $('#send_cc_address').val();
	json_email_resp.subject = $('#send_subject').val();
	json_email_resp.attachments = $('#send_attachments').val();
	//json_email_resp.from = 'hscott@hme.com';
	console.log(json_email_resp);
	send_json_email(JSON.stringify(json_email_resp));
}

var email_spinner;
function send_json_email(data){
	//console.log(data);
	$('.email-send-button').addClass('email_send_btn_active');
	email_spinner = document.getElementById('email_header');
	var opts = spinnerOptions();
	email_spinner = new Spinner(opts).spin(email_spinner);
	$.ajax({
		type: "POST",
		contentType: "application/json; charset=utf-8",
		url: "ajax/send_email.cfm",
		data: data,
		dataType: "json",
		success: function (data) {					
			email_spinner.stop();
			$('.email-send-button').removeClass('email_send_btn_active');
			if (data != null) {
				if(data.results == "OK"){
					$('#rte_modal').modal('hide');
					display_status_message('email sent');
				}
				else{
					display_email_validation(data.results);
				}
				console.log(data);
			}
			else{
			}
		},
		error: function (jqXHR, ajaxOptions, thrownError) {
			email_spinner.stop();
			$('.email-send-button').removeClass('email_send_btn_active');
			$('#login_iframe').attr('src', 'login.cfm');
			$('#login_modal').modal('show');
			//setTimeout(function(){location.href=location.href;}, 300);	   
        }
	});	
}

function toggle_emp_select(obj){
	tr_obj = obj.closest('tr');
	if(tr_obj.hasClass('user-selected-color')){
		tr_obj.removeClass('user-selected-color');
		tr_obj.addClass('mouseover');
	}else{
		tr_obj.addClass('user-selected-color');
		tr_obj.removeClass('mouseover');
	}
}

function getOffset( el ) {
    var _x = 0;
    var _y = 0;
    while( el && !isNaN( el.offsetLeft ) && !isNaN( el.offsetTop ) ) {
        _x += el.offsetLeft - el.scrollLeft;
        _y += el.offsetTop - el.scrollTop;
        el = el.offsetParent;
    }
    return { top: _y, left: _x };
}

function autocomplete(val){
	 var searchReq = 'fn';
	switch($('.search-request-types input[name=search_request]:checked').val()){
		case 'by_firstname':
		 	searchReq = 'givenname';
			break;
		case 'by_lastname':
		 	searchReq = 'sn';
			break;
		break;
		case 'by_department':
		 	searchReq = 'department';
		break;
	}
	console.log($('.search-request-types input[name=search_request]:checked').val());
	var res = alasql('select cn,mail,department,office,givenname,sn from ? where ' + searchReq + ' like "' + val + '%"  order by ' + searchReq + ' asc',[json_hme_users]);    
	build_address_book(res, searchReq);
}

var contacts = '';	
function build_address_book(json_list, searchReq){
	$('#email_contact_book').html('');	
	contacts =  '<table class="emp emp_x">';
	contacts += '<tr style="position:absolute; background-color:#fff; width:579px;">';
	contacts += '<td class="col-padding right-border bottom-border" style="width:203px;">Name</td>';
	contacts += '<td class="col-padding bottom-border" style="width:200px;">Department</td>';
	contacts += '<td class="col-padding left-border bottom-border" style="width:200px;">Location</td></tr>';
	contacts += '<tr><td style="width:200px;">&nbsp;</td>';
	contacts += '<td style="width:200px;">&nbsp;</td>';
	contacts += '<td style="width:200px;">&nbsp;</td></tr>';
	$.each(json_list, function(index, item) {
		contacts += '<tr id="tr-emp_' + index + '" class="tr-emp"  email="'+ item.cn + '<' + item.mail + '>">';
		if(searchReq == 'sn')
			contacts += '<td id="cd-emp_' + index + '" class="cn emp-td col-padding right-border" style="width:200px;">' + item.sn + ' '  + item.givenname + '</td>';
		else
			contacts += '<td id="cd-emp_' + index + '" class="cn emp-td col-padding right-border" style="width:200px;">' + item.givenname + ' '  + item.sn + '</td>';
		contacts += '<td id="dept-emp_' + index + '" class="dept emp-td col-padding" style="width:200px;">' + item.department + '</td>';
		contacts += '<td id="loc-emp_' + index + '" class="loc emp-td col-padding left-border" style="width:200px;">' + item.office + '</td></tr>';
	});
	contacts += '</table>';	
	$('#email_contact_book').append(contacts);
	set_contact_properties();	

	//console.log($('#email_contact_book').innerWidth())
	//console.log($('.emp_x').css('width'))
}

function display_email_validation(msg){
	//var modal_width = parseInt($(".modal-content").css('width'),10);
	//console.log($(".modal-content").css('width'));
	//console.log(modal_width);
	//console.log($('.email-validatrion-error').width());
	//var w = (modal_width - $('.email-validatrion-error').width()) /2;
	$('.email-validatrion-error').css('left','60px');
	$('.email-validatrion-error .error-message').html(msg);
	$('.email-validatrion-error .expandable-element').height($('.email-validatrion-error .error-message').prop('scrollHeight')+2);
	$('.email-validatrion-error .email_close_btn').show();
}

function close_email_validation(){
	$('.email-validatrion-error .expandable-element').height(0);
	$('.email-validatrion-error .email_close_btn').hide();
}