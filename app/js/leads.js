var excel_json = [];
var lead_excluded = [];
var display_lead_index
function displayLead(cust){
	display_lead_index = json.map(function (assetid) { return assetid['assetid']}).indexOf(cust);
	show_lead(display_lead_index)
}

function show_lead(index){
	var lead_index = lead_excluded.map(function (assetid) { return assetid['assetid']; }).indexOf(json[index].assetid);
	$.each(json[index], function(key, value) {
		if(key != 'sortcolumn' && key != 'sortorder'){
			$('#display_' + key).html(value);
		}
    });

	$('#excel_exclude').attr('onclick','lead_ckbox_clicked(this,"' + json[index].assetid + '")');
	$('#excel_exclude').prop('checked', (lead_index >= 0 ? true : false));	
	$('.display-lead-button').prop('disabled', false);
	if(index == 0 && $('#CSP_Current_Page').val() == '1')
		$('#Lead_Prev_Page').prop('disabled', true);
		
	if((index + 1) == json.length && ($('#CSP_Per_Page').val() * $('#CSP_Current_Page').val()) >= lead_count)
		$('#Lead_Next_Page').prop('disabled', true);

	$('#display_lead_modal').modal('show');	
}

function excel_order(obj, msg){
	var excel_json = [];
	obj.each(function(i, items_list){
        $(items_list).find('li').each(function(j, li){
			var excel_item = {}, column = $(li);
			if(column.children('input[type=checkbox]').prop('checked')){
				excel_item.column = column.children('div:first').html();
				excel_item.name = column.children('input[type=text]').val() == '' ? column.children('div:first').html() : column.children('input[type=text]').val();
				excel_json.push(excel_item);
			}
        });
    });
	$('#excel_columns').val(JSON.stringify(excel_json));
	$('#excel_save').html(msg);
	$('#excel_save_btn').hide();
	$('#show_excel_header').val($('input[name=excel-header]').val());
	console.log(excel_json);
}

function sort_results(obj){	
	console.log(obj);
	$('.header_title').removeClass('sorted');
	$('#' + obj.prop('id') + ' span:first-child').addClass('sorted');
	if(obj.hasClass('sort') || obj.hasClass('desc')){
		$(".sortable").removeClass('sort').removeClass('desc').removeClass('asc');																												
		$(".sortable").addClass('sort');														
		obj.removeClass('sort').addClass('asc');
		$('#Sort_Order').val('asc');
	}else{
		$('.sortable').removeClass('sort').removeClass('desc').removeClass('asc');																												
		$('.sortable').addClass('sort');														
		obj.removeClass('sort').addClass('desc');
		$('#Sort_Order').val('desc');
	}
	$('#Sort_Column').val(obj.prop('id'));
	$('#CSP_Current_Page').val('1');
	var json_update = {};
	var leadFilter_update = JSON.parse(leadFilter);
	leadFilter_update.itemsperpage = $('#CSP_Per_Page').val();
	leadFilter_update.pagenumber = $('#CSP_Current_Page').val();
	leadFilter_update.sortorder = $('#Sort_Order').val();
	var sort_col = '';
	switch( $('#Sort_Column').val()){
		case 'h_assetid':
			sort_col = '[Asset/ ID]';
			break;
		case 'h_area':
			sort_col = '[area]';
			break;
		case 'h_type':
			sort_col = '[type]';
			break;
		case 'h_status':
			sort_col = '[status]';
			break;
		case 'h_model':
			sort_col = '[model]';
			break;
		case 'h_manufacturer':
			sort_col = '[manufacturer]';
			break;
		case 'h_lastdone':
			sort_col = '[lastdone]';
			break;
		case 'h_nextdue':
			sort_col = '[nextdue]';
			break;
		case 'h_testprocedure':
			sort_col = '[test procedure]';
			break;
		case 'h_serial':
			sort_col = '[serial]';
			break;
		case 'h_category':
			sort_col = '[category]';
			break;
		case 'h_daysold':
			sort_col = '[daysold]';
			break;
		case 'h_technician':
			sort_col = '[technician]';
			break;
		case 'h_location':
			sort_col = '[location]';
			break;
		case 'h_interval':
			sort_col = '[interval]';
			break;
	}
	leadFilter_update.sortcolumn = sort_col;
	json_update.leadFilter = leadFilter_update;
	console.log(leadFilter_update);
	page_fai_ajax(JSON.stringify(leadFilter_update));
}

var page_spinner;
function page_fai_ajax(data){
	page_spinner = document.getElementById('fai_search_spinner');
	var opts = spinnerOptions();
	page_spinner = new Spinner(opts).spin(page_spinner);
	$.ajax({
		type: "POST",
		contentType: "application/json; charset=utf-8",
		url: "ajax/page_fai.cfm",
		data: data,
		dataType: "json",
		success: function (data, textStatus, jqXHR ) {					
			page_spinner.stop();
			if (data != null) {
				console.log(data);
				display_selected_leads(data);
			}
		},
		error: function (jqXHR, ajaxOptions, thrownError) {
			page_spinner.stop();
			$('#login_iframe').attr('src', 'login.cfm');
			$('#login_modal').modal('show');
			//setTimeout(function(){location.href=location.href;}, 300);
        }
	});
}
//textStatus, errorThrown
function display_selected_leads(fais){
	var tbody = '';
	var lead_index;
	$.each(fais.returnselect, function(index, fai) {
		tbody += '<tr>';
		tbody += '<td><a href="javascript:void(0);" onclick="show_fai_from_filter(\'' + fai.assetid + '\');">' + fai.assetid + '</a></td>';
		tbody += '<td>' + fai.area  + '</td>';
		tbody += '<td>' + fai.type  + '</td>';
		tbody += '<td>' + fai.status  + '</td>';
		tbody += '<td>' + fai.manufacturer  + '</td>';
		tbody += '<td>' + fai.model  + '</td>';
		tbody += '<td>' + fai.serial  + '</td>';
		tbody += '<td>' + (fai.lastdone == '' ? '' : $.date(new Date(fai.lastdone)))  + '</td>';
		tbody += '<td>' + (fai.nextdue == '' ? '' : $.date(new Date(fai.nextdue)))  + '</td>';
		tbody += '<td>' + fai.category  + '</td>';
		tbody += '<td style="text-align:center;">' + fai.interval  + '</td>';
		tbody += '<td style="text-align:center;">' + fai.daysold  + '</td>';
		tbody += '<td>' + fai.technician  + '</td>';
		tbody += '<td style="text-align:center;">' + fai.location  + '</td>';
    });
	$('#Cust_Results tbody').empty();
	$('#Cust_Results tbody').html(tbody);
	json = fais.returnselect;
	leadFilter = JSON.stringify(fais.faifilter);
	console.log(leadFilter);
	$('#CSP_Current_Page').val(fais.pagenumber);
	$('.csp-page-start').html(fais.page_start);
	$('.csp-page-end').html(fais.page_end);
	$('.total_leads').html(fais.count);
	lead_count = fais.count;
	console.log(lead_count);
	$('#CSP_Prev_Page').prop('disabled',false);
	$('#CSP_Next_Page').prop('disabled',false);
	if( $('#CSP_Current_Page').val() == '1')
		$('#CSP_Prev_Page').prop('disabled',true);
	else
		if(($('#CSP_Per_Page').val() * $('#CSP_Current_Page').val()) >= lead_count)
			$('#CSP_Next_Page').prop('disabled',true);
	
	if(lead_display_direction != 0 && $('#display_lead_modal').hasClass('in')){
		if(lead_display_direction < 0){
			display_lead_index = json.length - 1;
			show_lead(display_lead_index);
			lead_display_direction = 0;
		}
		else{
			display_lead_index = 0;
			show_lead(display_lead_index);
			lead_display_direction = 0;
		}
	}
	
}

$.date = function(dateObject) {
    var d = new Date(dateObject);
    var day = d.getDate();
    var month = d.getMonth() + 1;
    var year = d.getFullYear();
    if (day < 10) {
        day = "0" + day;
    }
    if (month < 10) {
        month = "0" + month;
    }
    var date = month + "/" + day + "/" + year;

    return date;
};

var lead_display_direction = 0;
function lead_paging(direction){
	lead_display_direction = direction;
	var json_update = {};
	var leadFilter_update = JSON.parse(leadFilter);
	console.log(leadFilter);
	leadFilter_update.itemsperpage = $('#CSP_Per_Page').val();
	leadFilter_update.pagenumber = (parseInt($('#CSP_Current_Page').val()) + direction).toString();
	json_update.leadFilter = leadFilter_update;
	console.log(json_update)
	page_fai_ajax(JSON.stringify(leadFilter_update));
}

function ckbox_clicked(obj, id){
	var jsonObj = {};
	var index = json.map(function (id) { return id['id']; }).indexOf(id);
	var lead_index = lead_excluded.map(function (id) { return id['id']; }).indexOf(id);
	if(obj.checked){
		$(obj).closest('tr').addClass('excel-excluded')
		json[index].exclude = '1';
		jsonObj.id = id;
		lead_excluded.push(jsonObj);
	}else{
		$(obj).closest('tr').removeClass('excel-excluded')
		json[index].exclude = '0';
		lead_excluded.splice(lead_index,1);
	}
}

function lead_ckbox_clicked(obj,id){
	var jsonObj = {};
	var index = json.map(function (id) { return id['id']; }).indexOf(id);
	var lead_index = lead_excluded.map(function (id) { return id['id']; }).indexOf(id);
	if(obj.checked){
		$('#tr' + id).addClass('excel-excluded');
		$('#ck' + id).prop('checked', true);
		json[index].exclude = '1';
		jsonObj.id = id;
		lead_excluded.push(jsonObj);
	}else{
		$('#tr' + id).removeClass('excel-excluded')
		$('#ck' + id).prop('checked', false);
		json[index].exclude = '0';
		lead_excluded.splice(lead_index,1);
	}
}

function fai_search_submit(){
	$('#search_results_type').html('Search Results');
	var s_json = {};
	var id= '';
	var input;
	$("#fai_Search_Form input[type=text]").each(function(index){
	 	input = $(this);
		id = input.prop("id").replace('s_','');
		s_json[id] = input.val(); 
	});
	$("#fai_Search_Form select").each(function(index){
	 	input = $(this);
		id = input.prop("id").replace('s_','');
		s_json[id] = input.val(); 
	});
	s_json.itemsperpage = $('#CSP_Per_Page').val();
	s_json.pagenumber = '1';
	s_json.custom = 'search';
	s_json.sortorder = $('#Sort_Order').val();
	s_json.sortcolumn = $('#Sort_Column').val();
	leadFilter = JSON.stringify(s_json);

	console.log(s_json);

	page_fai_ajax(leadFilter);
	return false;
}

function export_filter(type){
	console.log(leadFilter);
	var export_filter = JSON.parse(leadFilter);
	export_filter.itemsperpage = '5000';	
	console.log(export_filter);
	clear_pdf_form();
	$('#fai_report_storedProcedure').val('fai_select');
	$('#fai_report_req').val('export_filter_' + type);
	$('#fai_filters').val(JSON.stringify(export_filter));
	console.log($('#fai_report_req').val());
	
	$('#pdfReport').submit();
}