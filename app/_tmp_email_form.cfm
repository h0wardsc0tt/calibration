
<div id="rte_modal" class="modal fade" role="dialog" data-backdrop="static">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center">
            <div class="modal-content" style="width:800px;height: auto;">
                <div id="email_header" class="modal-header">
                    <div style="color:#fff; text-align:left;">
                        <i style="font-size:23px;" class="fa fa-envelope"></i>
                        <div style="display:inline-block; padding-left:10px; font-size:20px;">FAI Email</div>
                    </div>
                </div>
                
                 
                <div class="modal-body text-center" style="padding:5px 5px 5px 5px;">                
                    <div class="row" style="vertical-align:top;text-align:left; margin-right:0;">
                        <div style="vertical-align:top;" class="col-xs-1">
                            <div onClick="send_message()" class="email-send-button email-send-out" 
                             	title="Send Email"
                                onMouseOut="$(this).removeClass('email-send-over').addClass('email-send-out');" 
                                onMouseOver="this.style.cursor='pointer';$(this).removeClass('email-send-out').addClass('email-send-over');" 
                                style="height:45px; width:45px; display:inline-block;text-align:center;font-size:12px;">
                                <div style="padding-top:15px;">Send</div>
                            </div>
                        </div>
					<div style="position:relative;">
                    <div class="email-validatrion-error">
                        <div class="expandable-element" style="height: 0px;">
                            <div style="border:solid 1px #ff0000;color:#1910EF">
                                <div class="email_close_btn" onMouseOver="this.style.cursor='pointer';"><i title="close" 
                                style="color:#dddddd" onClick="close_email_validation();" class="fa fa-times"></i></div>
                                <div style="padding:10px 5px 10px 5px; text-align:left;" class="error-message">You must provide a security question answer</div>
                             </div>
                        </div>
                    </div>                    
					</div>
                        
                        <div style="display:inline-block;line-height: 1.3; font-size:12px;" class="col-xs-11">
                            <div class="row">
                                <div valign="top" class="col-xs-1 email-no-padding" onclick="showAddress_book();">
                                    <div onMouseOver="this.style.cursor='pointer';" title="HME Employee Directory" valign="top" class="to-box">To..</div>
                                </div>
                                <div class="col-xs-11 email-no-padding">
                                    <textarea id="send_to_address" class="email_textarea email-to" spellcheck='false' data-autoresize rows="1" style="min-height: 20px; width:100%;"></textarea>
                                </div>                                                                                                
                            </div>
                             <div class="row" style="margin-top: 4;">
                               <div valign="top" class="col-xs-1 email-no-padding" onclick="showAddress_book();">
                                    <div onMouseOver="this.style.cursor='pointer';" title="HME Employee Directory" valign="top" class="to-box">Cc..</div>
                                </div>
                                <div class="col-xs-11 email-no-padding">
                                    <textarea id="send_cc_address" class="email_textarea email-to" spellcheck='false' data-autoresize rows="1" style="min-height: 20px; width:100%;"></textarea>
                                </div>                                 
                            </div>
                            <div class="row" style="margin-top: 4;">
                                <div onMouseOver="this.style.cursor='pointer';" valign="top" class="col-xs-1 email-no-padding">
                                    <div valign="top" class="subject-box">Subject</div>
                                </div>
                                <div class="col-xs-11 email-no-padding">
                                    <textarea id="send_subject" class="email_textarea email-to" spellcheck='false' data-autoresize rows="1" style="min-height: 20px; width:100%;"></textarea>
                                </div>                                 
                            </div>
                            <div class="row" style="margin-top: 4;margin-bottom: 4;">
                                <div valign="top" class="col-xs-1 email-no-padding">
                                    <div valign="top" class="attached-box">Attached</div>
                                </div>
                                <div class="col-xs-11 email-no-padding">
                                    <textarea id="send_attachments" class="email_textarea email-to"  spellcheck='false' data-autoresize rows="1" style="min-height: 20px; width:100%;">hscott@hme.com;hscott@currentevent.com;foo@foo.com</textarea>
                                    <div id="email_attachments_list" class="email-to" style="display:none;position:absolute; border:solid 1px #ddd;min-height:auto; width:100%; margin-top:1px; z-index:9000;background-color:#fff;">
                                        <ul id="email_ul_attachments_list" style="padding:0; margin:3px 3px 3px 3px; border:solid 1px #337ab7;">
                                            <li>
                                                <div style="width:15px; display:inline-block;">
                                                    <i class="attachment_add_remove fa fa-check attachment-attach"></i>
                                                </div>
                                                <span>hscott@hme.com</span>
                                            </li>
                                             <li>
                                                 <div style="width:15px; display:inline-block;">
                                                    <i class="attachment_add_remove fa fa-check attachment-attach"></i>
                                                 </div>
                                                 <span>hscott@currentevent.com</span>
                                             </li> 
                                             <li>
                                                 <div style="width:15px; display:inline-block;">
                                                    <i class="attachment_add_remove fa fa-check attachment-attach"></i>
                                                 </div>
                                                 <span>foo@foo.com</span>
                                             </li>                                      
                                         </ul>                               
                                    </div>
                                </div>  
                                <div id="email_contacts" class="email-container">
                                    <div id="email_contact_header" style="height:25px;width:100%; background-color:#337ab7; color:#fff; padding:6px 0px 0px 5px;">Address List</div>
                                    <div style="padding:0px 3px 0px 3px;">
                                    <div id="email_contact_search" style="height:25px;width:100%;margin:4px 0px 0px 0px;padding:0px 0px 0px 0px;">
                                        <div style="display:inline-block;">
                                        	<input id="address_book_rb" class="search-type" value="address_book" name="search_type" type="radio" checked>
                                        </div>
                                       <div style="display:inline-block; vertical-align:bottom; padding-bottom:2px;margin-right:20px;">
                                        	Address List
                                        </div>

                                      	<div style="display:inline-block;">
                                        	<input id="address_book_search_rb" class="search-type" value="address_search" name="search_type" type="radio">
                                        </div>
                                       <div id="list_search" style="display:inline-block; vertical-align: bottom;margin-right:10px;">
                                        	Search:<input id="search_address_book" disabled type="text" style="width:100px;">
                                        </div>
                                       <div class="search-request-types" style="display:inline-block;">                                       
                                           <div style="display:inline-block;">
                                                <input id="address_book_rb" value="by_firstname" class="search-request" name="search_request" type="radio" checked>
                                            </div>
                                           <div style="display:inline-block; vertical-align:bottom; padding-bottom:2px;margin-right:5px;">
                                                By First Name
                                            </div>
                                           <div style="display:inline-block;">
                                                <input id="address_book_rb" value="by_lastname" class="search-request" name="search_request" type="radio">
                                            </div>
                                           <div style="display:inline-block; vertical-align:bottom; padding-bottom:2px;margin-right:5px;">
                                                By Last Name
                                            </div>
                                           <div style="display:inline-block;">
                                                <input id="address_book_rb" value="by_department" class="search-request" name="search_request" type="radio">
                                            </div>
                                           <div style="display:inline-block; vertical-align:bottom; padding-bottom:2px;margin-right:5px;">
                                                By Department
                                            </div>
										</div>

                                        
                                    </div>
                                    <div id="email_contact_book" style="height:250px;width:600px;margin:0px 0px 0px 0px; background-color:#fff; padding:0px 0px 0px 5px; overflow-y: scroll;">
                         
                                    </div>
                                    <div id="email_contact_selected" style="height:50px;width:100%;margin:5px 0px 5px 0px;  background-color:#EFEDED; padding:0px 0px 0px 5px;">
                                       <div class="row">
                                           <div class="col-xs-12">
                                                <div valign="top" class="col-xs-2 email-no-padding">
                                                    <button id="get_to_address" type="button" class="btn btn-default" style="height:20px;padding:0;font-size:12px; width:80px;">To -></button>
                                                </div>
                                                <div class="col-xs-10 email-no-padding">
                                                    <textarea id="email_to_selected" class="email-to" spellcheck='false' data-autoresize rows="1" style="min-height: 20px; width:100%;"></textarea>
                                                </div>                                 
                                            </div>
                                      </div>
                                      <div class="row" style="margin-top:5px;">
                                           <div class="col-xs-12">
                                                <div valign="top" class="col-xs-2 email-no-padding">
                                                    <button id="get_cc_address" type="button" class="btn btn-default" style="height:20px;padding:0;font-size:12px; width:80px;">Cc -></button>
                                                </div>
                                                <div class="col-xs-10 email-no-padding">
                                                    <textarea id="email_cc_selected" class="email-to" spellcheck='false' data-autoresize rows="1" style="min-height: 20px; width:100%;"></textarea>
                                                </div>                                 
                                            </div>
                                       </div>

										<div class="row" style="margin-top:5px;">
                                           <div class="col-xs-12">
                                                <div  style="text-align:right;">
                                                               <button id="address_book_ok_btn" type="button" class="btn btn-default" style="height:20px;padding:0;font-size:12px; width:80px;">OK</button>
                                                               <button id="address_book_cancel_btn" type="button" class="btn btn-default" style="height:20px;padding:0;font-size:12px; width:80px;">Cancel</button>
                                                </div>
                                            </div>
                                       </div>

                                    </div> 
                                    </div>                                
                                </div>                             
                            </div>
                       </div>
                    </div>
                    <div class="row">                   
                        <div class="col-xs-12">
                            <textarea type="text" id="modal_rte_textarea" name="modal_rte_textarea" class="modal_rte_textarea"
                            style="overflow-y:auto;height:300px; width:699px;margin-top:5px; border:solid 1px #B4AFAF;"></textarea>
                        </div>
                    </div>
           		</div>
                <div class="modal-footer email-footer-buttons">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">

</script>

