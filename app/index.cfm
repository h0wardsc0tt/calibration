<cfsetting showdebugoutput="no">

<cfparam name="URL.asset" default="">
<cfparam name="URL.pg" default="">
<cfparam name="URL.st" default="">
<cfparam name="URL.store" default="">
<cfparam name="User_IsLoggedIn" default="false">

<!--- Verify Session --->
<cfset assetid = "">
<cfif URL.asset neq "">
	<cfset assetid = "&asset=#URL.asset#">
</cfif>

<cfif 
	StructKeyExists(SESSION, "Session_UID") 
	AND 
	StructKeyExists(SESSION, "User_UID")
	AND 
	StructKeyExists(SESSION, "IsLoggedIn")>
	
	<cfscript>
		checkSession = CreateObject("component", "_cfcs.SessionMgmt");
		statsSession = checkSession.verifySession(Session_UID=SESSION.Session_UID,User_UID=SESSION.User_UID);
	</cfscript>
	
	<cfif statsSession EQ 1>
        <cfset User_IsLoggedIn = true>
	</cfif>
<cfelse>
	<cfset noRedirect = "Login">
	<cfif NOT ListFind(noRedirect, URL.pg)>
		<cflocation url="./?pg=Login#assetid#" addtoken="no">
		<cfabort>
	</cfif>
</cfif>

<cfinclude template="./_tmp_HTML_Header.cfm">

<!--- Check if User is Logged in --->
<cfif User_IsLoggedIn>
	<cfinclude template="./_tmp_HTML_Navigation_LoggedIn.cfm">
</cfif>

<cfswitch expression="#URL.pg#">
    <cfcase value="Login">
		<cfif NOT User_IsLoggedIn>
            <cfinclude template="./security/_dat_login_prep.cfm">
            <cfswitch expression="#URL.st#">
                <cfcase value="Validate">
                    <cfinclude template="./security/_tmp_login_check.cfm">
                </cfcase>
                <cfdefaultcase>
                    <cfinclude template="./security/_tmp_login_form.cfm">
                </cfdefaultcase>
            </cfswitch>
        <cfelse>
        	<cfinclude template="./security/_tmp_logout.cfm">
        </cfif>
    </cfcase>
    <cfcase value="Logout">
        <cfinclude template="./security/_tmp_logout.cfm">
    </cfcase>
    <cfcase value="Calibration">
		<cfinclude template="./serverSettings.cfm">
		<cfinclude template="./_tmp_fai_display.cfm">
		<cfinclude template="./_tmp_HTML_Footer.cfm">
		<cfinclude template="./_tmp_fai_modal.cfm">
    </cfcase>

    <cfdefaultcase>
    	<cfif NOT User_IsLoggedIn>
            <cflocation url="./?pg=Login#assetid#" addtoken="no">
            <cfabort>
        <cfelse>
        	<cflocation url="./?pg=Calibration#assetid#" addtoken="no">
        </cfif>
    </cfdefaultcase>
</cfswitch>
