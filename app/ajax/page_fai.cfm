<cfinclude template="../serverSettings.cfm">
<cfsetting showdebugoutput="no">
<cfscript>		
	if (#FindNoCase('application/json', cgi.content_type)# > 0)
	{
    	faiFilter = deserializeJSON(ToString(getHTTPRequestData().content));
		jsonFAI = StructNew();
		jsonFAI['pageNumber'] = faiFilter.pagenumber;
		jsonFAI['itemsPerPage'] = faiFilter.itemsperpage;
		if(faiFilter.sortcolumn != ''){
			jsonFAI['sortcolumn'] = faiFilter.sortcolumn;
			jsonFAI['sortorder'] = faiFilter.sortorder;
		}
		obj = CreateObject("component", "#cfcs#.calibration");
		switch(faiFilter.custom){
			case 'assetidLookup':
				jsonFAI['custom'] = faiFilter.custom;	
				jsonFAI['assetid'] = faiFilter.assetid;
				returnSelect  = obj.custom(argumentCollection = jsonFAI);
				break;
			case 'default':
				jsonFAI['custom'] = faiFilter.custom;	
				returnSelect  = obj.custom(argumentCollection = jsonFAI);
				break;
			case 'search' :
				jsonFAI['custom'] = faiFilter.custom;	
				if(faiFilter.lastdone_from != '')
					jsonFAI['lastdone_from'] = faiFilter.lastdone_from;
				if(faiFilter.lastdone_to != '')
					jsonFAI['lastdone_to'] = faiFilter.lastdone_to;
				if(faiFilter.nextdue_from != '')
					jsonFAI['nextdue_from'] = faiFilter.nextdue_from; 
				if(faiFilter.nextdue_to != '')
					jsonFAI['nextdue_to'] = faiFilter.nextdue_to;
				jsonFAI['area'] = faiFilter.area;
				jsonFAI['assetid'] = faiFilter.assetid;
				jsonFAI['category'] = faiFilter.category;
				jsonFAI['manufacturer'] = faiFilter.manufacturer;
				jsonFAI['model'] = faiFilter.model;
				jsonFAI['serial'] = faiFilter.serial;
				jsonFAI['status'] = faiFilter.status;
				jsonFAI['type'] = faiFilter.type;
				jsonFAI['location'] = faiFilter.location;
				jsonFAI['approve'] = faiFilter.approve;
				
				returnSelect  = obj.custom(argumentCollection = jsonFAI);
				break;
		}

		returnResult = StructNew();
		returnResult['itemsperpage'] = faiFilter['itemsperpage'];
		returnResult['pagenumber'] = faiFilter['pagenumber'];

		if(returnSelect.RecordCount > 0)
			returnResult['count'] = returnSelect['count'];
		else
			returnResult['count'] = '0';
		deleteStatus = StructDelete(faiFilter,'storedProcedure','True');
		returnResult['returnSelect'] = returnSelect;	
		returnResult['faiFilter'] = StructCopy(faiFilter);	
		returnResult['page_start'] = ((returnResult['pagenumber'] - 1) * returnResult['itemsperpage']) + 1;	
		returnResult['page_end'] = returnResult['pagenumber'] * returnResult['itemsperpage'];	
 		StructClear(faiFilter);	
	}
	else
	{
		returnResult = StructNew();
	}
	
	serializer = new lib.JsonSerializer();
	writeOutput(serializer.serialize(returnResult));	
</cfscript>
