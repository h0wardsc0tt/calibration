<cfcomponent 
	displayname="Session" 
	output="yes" 
	hint="Initialize User Session">
	
	<cfparam name="THIS.SessionTimeout" default="#APPLICATION.SessionTimeout#">
	
	<cffunction 
		name="initSession" 
		returntype="void" 
		hint="Initializes Session">
		
		<cfargument name="User_UID" required="yes" type="string">

		<!---Get SESSION_UID--->
		<cflock scope="session" type="exclusive" timeout="#THIS.SessionTimeout#">
			<cfinvoke 
				method="getSession_UID" 
				returnvariable="Session_UID"></cfinvoke>
			<cfset SESSION.Session_UID = Session_UID>
		</cflock>
		
		<!---Track Session--->
		<cfinvoke method="TrackSession">
			<cfinvokeargument name="Session_UID" value="#SESSION.Session_UID#">
			<cfinvokeargument name="User_UID" value="#User_UID#">
		</cfinvoke>
	</cffunction>
	<cffunction 
		name="verifySession" 
		returntype="numeric" 
		hint="Renews Current Session">
		
		<cfargument name="Session_UID" type="string" required="yes">
		<cfargument name="User_UID" type="string" required="yes">
		<cfparam name="OKGO" default="0">
        
        <cfset CurrentDateTime = DateFormat(Now(),'yyyy-mm-dd') & ' ' & TimeFormat(Now(),'HH:mm:ss')>
        
        <!--- Query Permission Table to check if user is admin
		****************************************************** --->
		
		<!---Verify Session--->
		<cfscript>
			QUERY = StructNew();//Only retrieve user information for valid sessions
			QUERY.QueryName = "qry_getSessionInfo";
			QUERY.Datasource = APPLICATION.Datasource;
			QUERY.User_UID = User_UID;
			QUERY.Session_UID = Session_UID;
			QUERY.User_IP = CGI.REMOTE_ADDR;
			//QUERY.Session_Timeout = CreateODBCDateTime(DateAdd("n", -25, Now()));
			QUERY.Session_Timeout = "#DateFormat(DateAdd('n', -25, Now()),'yyyy-mm-dd')# #Hour(DateAdd('n', -25, Now()))#:00:00";
		</cfscript>
		<cfquery name="#QUERY.QueryName#" datasource="#QUERY.Datasource#" maxrows="1">
			SELECT sess.User_Session_UID, sess.User_UID
			FROM
				dbo.dtbl_User_Session sess 
			WHERE 0=0
			AND sess.User_Session_UID = <cfqueryparam cfsqltype="cf_sql_varchar" value="#QUERY.Session_UID#">
			AND sess.User_UID = <cfqueryparam cfsqltype="cf_sql_varchar" value="#QUERY.User_UID#">
			AND sess.User_IP = <cfqueryparam cfsqltype="cf_sql_varchar" value="#QUERY.User_IP#">
			AND sess.User_DTS >= <cfqueryparam cfsqltype="cf_sql_varchar" value="#QUERY.Session_Timeout#">
			ORDER BY sess.User_DTS DESC
		</cfquery>
		<cfif StructClear(QUERY)></cfif>
		
		<cfif qry_getSessionInfo.RecordCount NEQ 0>
			<cflock scope="session" type="exclusive" timeout="#THIS.SessionTimeout#">
			<cfscript>
				SESSION.IsLoggedIn = true;
				SESSION.loginAttempts = 0;
				SESSION.Session_UID = qry_getSessionInfo.User_Session_UID;
				SESSION.User_UID = qry_getSessionInfo.User_UID;
			</cfscript>
			</cflock>
			<cfquery name="qry_ExtendSession" datasource="#APPLICATION.Datasource#">
				INSERT INTO dbo.dtbl_User_Session
					(User_Session_UID,
					User_UID,
					User_IP,
					User_DTS,
                    User_Agent)
				VALUES
					(<cfqueryparam cfsqltype="cf_sql_varchar" value="#SESSION.Session_UID#">,
					<cfqueryparam cfsqltype="cf_sql_varchar" value="#SESSION.User_UID#">,
					<cfqueryparam cfsqltype="cf_sql_varchar" value="#CGI.REMOTE_ADDR#">,
					<cfqueryparam cfsqltype="cf_sql_varchar" value="#CurrentDateTime#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#CGI.HTTP_USER_AGENT#">)
			</cfquery>
			<cfscript>
				QUERY = StructNew();
				QUERY.QueryName = "qry_InsertTracking";
				QUERY.Datasource = APPLICATION.Datasource;
			</cfscript>
		
			<cfquery name="#QUERY.QueryName#" datasource="#QUERY.Datasource#">
				INSERT INTO dbo.dtbl_Audit_User
					(Audit_User_UID,
					Audit_LastLogin,
					Audit_LastDomain,
					Audit_LastIP,
					Audit_LastAgent,
					Audit_Action,Audit_User_Department)
				VALUES
					(<cfqueryparam cfsqltype="cf_sql_varchar" value="#qry_getSessionInfo.User_UID#">,
					<cfqueryparam cfsqltype="cf_sql_varchar" value="#CurrentDateTime#">,
					<cfqueryparam cfsqltype="cf_sql_varchar" value="#CGI.HTTP_HOST#">,
					<cfqueryparam cfsqltype="cf_sql_varchar" value="#CGI.REMOTE_ADDR#">,
					<cfqueryparam cfsqltype="cf_sql_varchar" value="#CGI.HTTP_USER_AGENT#">,
					<cfqueryparam cfsqltype="cf_sql_longvarchar" value="#CGI.QUERY_STRING#">,
					<cfqueryparam cfsqltype="cf_sql_varchar" value="#SESSION.User_Dept#">)
			</cfquery>
			<cfif StructClear(QUERY)></cfif>
			
			<cfset OKGO = 1>
		</cfif>
        
        <cfset OKGO = 1>
		
		<cfreturn OKGO>
	</cffunction>
	<cffunction 
		name="getSession_UID" 
		returntype="string" 
		access="public"
		hint="Gets new Session_UID">
		
		<cfscript>
			getUID = CreateObject("component", "_cfcs.Generate_UID");
			thisSESSION_UID = getUID.genUID(Length=32);
		</cfscript>
		
		<cfreturn thisSESSION_UID>
	</cffunction>
	
    <!--- COMMENT OUT DURING DEVELOPMENT TO BYPASS LOGIN --->
	<cffunction 
		name="TrackSession" 
		returntype="void" 
		hint="Creates DB Record" 
		access="public">
		
		<cfargument name="Session_UID" type="string" required="yes">
		<cfargument name="User_UID" type="string" required="yes">
        
        <cfset CurrentDateTime = DateFormat(Now(),'yyyy-mm-dd') & ' ' & TimeFormat(Now(),'HH:mm:ss')>
		
		<cfscript>
			QUERY = StructNew();
			QUERY.QueryName = "qry_InsertTracking";
			QUERY.Datasource = APPLICATION.Datasource;
		</cfscript>

		<cfquery name="#QUERY.QueryName#" datasource="#QUERY.Datasource#">
			INSERT INTO dbo.dtbl_User_Session
				(User_Session_UID,
				User_UID,
				User_IP,
				User_DTS,
                User_Agent)
			VALUES
				(<cfqueryparam cfsqltype="cf_sql_varchar" value="#Session_UID#">,
				<cfqueryparam cfsqltype="cf_sql_varchar" value="#User_UID#">,
				<cfqueryparam cfsqltype="cf_sql_varchar" value="#CGI.REMOTE_ADDR#">,
				<cfqueryparam cfsqltype="cf_sql_varchar" value="#CurrentDateTime#">,
                <cfqueryparam cfsqltype="cf_sql_varchar" value="#CGI.HTTP_USER_AGENT#">)
		</cfquery>
		<cfif StructClear(QUERY)></cfif>
		
		<cfscript>
			QUERY = StructNew();
			QUERY.QueryName = "qry_InsertTracking";
			QUERY.Datasource = APPLICATION.Datasource;
		</cfscript>

		<cfquery name="#QUERY.QueryName#" datasource="#QUERY.Datasource#">
			INSERT INTO dbo.dtbl_Audit_User
				(Audit_User_UID,
				Audit_LastLogin,
				Audit_LastDomain,
				Audit_LastIP,
				Audit_LastAgent,
				Audit_Action,Audit_User_Department)
			VALUES
				(<cfqueryparam cfsqltype="cf_sql_varchar" value="#User_UID#">,
				<cfqueryparam cfsqltype="cf_sql_varchar" value="#CurrentDateTime#">,
				<cfqueryparam cfsqltype="cf_sql_varchar" value="#CGI.HTTP_HOST#">,
				<cfqueryparam cfsqltype="cf_sql_varchar" value="#CGI.REMOTE_ADDR#">,
				<cfqueryparam cfsqltype="cf_sql_varchar" value="#CGI.HTTP_USER_AGENT#">,
				<cfqueryparam cfsqltype="cf_sql_varchar" value="#CGI.QUERY_STRING#">,
				<cfqueryparam cfsqltype="cf_sql_varchar" value="#SESSION.User_Dept#">)
		</cfquery>
		<cfif StructClear(QUERY)></cfif>
		
	</cffunction>
	
	<cffunction 
		name="purgeSession" 
		returntype="void" 
		hint="Purges dtbl_User_Session of any events > Now()-1day">
		
		<cfargument name="RemoveDate" default="#DateAdd('d', -1, Now())#">
		
		<cfscript>
			QUERY = StructNew();
			QUERY.QueryName = "qry_delOldSessions";
			QUERY.Datasource = APPLICATION.Datasource;
			QUERY.fromDate = RemoveDate;
		</cfscript>
		
		<cfquery name="#QUERY.QueryName#" datasource="#QUERY.Datasource#">
			DELETE 
			FROM dbo.dtbl_User_Session
			WHERE User_DTS <= <cfqueryparam cfsqltype="cf_sql_date" value="#QUERY.fromDate#">
		</cfquery>
		<cfif StructClear(QUERY)></cfif>
	</cffunction>
    
	<cffunction 
		name="purgeReset" 
		returntype="void" 
		hint="Purges dtbl_Pass_Reset of any events > Now()-30day">
		
		<cfargument name="RemoveDate" default="#DateAdd('d', -30, Now())#">
		
		<cfscript>
			QUERY = StructNew();
			QUERY.QueryName = "qry_delOldResets";
			QUERY.Datasource = APPLICATION.Datasource;
			QUERY.fromDate = RemoveDate;
		</cfscript>
		
		<cfquery name="#QUERY.QueryName#" datasource="#QUERY.Datasource#">
			DELETE 
			FROM dbo.dtbl_Pass_Reset
			WHERE Reset_DTS <= <cfqueryparam cfsqltype="cf_sql_date" value="#QUERY.fromDate#">
		</cfquery>
		<cfif StructClear(QUERY)></cfif>
	</cffunction>
</cfcomponent>