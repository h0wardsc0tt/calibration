<!---<cfdump var="#SESSION#">--->
<cfparam name="URL.asset" default="">
<cfset assetid = "">
<cfif URL.asset neq "">
	<cfset assetid = "#URL.asset#">
</cfif>
<cfoutput>
	<div class="fai-header" align="center">TEST AND MEASUREMENT EQUIPMENT CALIBRATION - SEARCH SCREEN</div>

    <div id="fai_search_spinner" class="col-lg-12">
    	<h3 style="padding:0; margin:0;">Select Calibration Tests</h3>
    </div>    
	<!--- Customer Search Form --->
    <form action="" method="POST" id="fai_Search_Form" class="form-horizontal col-lg-12" role="form" onsubmit="return fai_search_submit()">
    	<div class="search_form_fields row">
            <div class="col-xs-4">
                <div class="form-group">
                    <label for="s_assetid" class="control-label col-xs-5">Asset Id:</label>
                    <div class="col-xs-7">
                        <input type="text" maxlength="100" name="s_assetid" id="s_assetid" class="form-control s-form"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="s_area" class="control-label col-xs-5">Area:</label>
                    <div class="col-xs-7">
                        <select class="level1 form-control col-xs-7 area" id="s_area" name="s_area">
                            <option value="" ></option>
						</select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="s_type" class="control-label col-xs-5">Type:</label>
                    <div class="col-xs-7">
                        <input type="text" maxlength="100" name="s_type" id="s_type" class="form-control s-form" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="s_model" class="control-label col-xs-5">Model:</label>
                    <div class="col-xs-7">
                        <input type="text" maxlength="100" name="s_model" id="s_model" class="form-control s-form" />
                    </div>
                </div> 
                <div class="form-group">
                    <label for="s_location" class="control-label col-xs-5">Location:</label>
                    <div class="col-xs-7">
                        <input type="text" maxlength="100" name="s_location" id="s_location" class="form-control s-form" />
                    </div>
                </div>
                
             </div>
            <div class="col-xs-4">
                <div class="form-group">
                    <label for="s_serial" class="control-label col-xs-5">Serial Number:</label>
                    <div class="col-xs-7">
                        <input type="text" maxlength="100" name="s_serial" id="s_serial" class="form-control s-form" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="s_lastdone_from" class="control-label col-xs-5">Last Done From:<div class="cal-cont"><span class="glyphicon glyphicon-calendar"></span></div></label>
                    <div class="col-xs-7">
                        <input type="text" maxlength="100" name="s_lastdone_from" id="s_lastdone_from" class="form-control cal-field s-form" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="s_nextdue_from" class="control-label col-xs-5">Next Due From:<div class="cal-cont"><span class="glyphicon glyphicon-calendar"></span></div></label>
                    <div class="col-xs-7">
                        <input type="text" maxlength="100" name="s_nextdue_from" id="s_nextdue_from" class="form-control cal-field s-form" value="#FORM.Lead_Email#" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="s_manufacturer" class="control-label col-xs-5">Manufacturer:</label>
                    <div class="col-xs-7">
                         <select class="level1 form-control col-xs-7 manufacturer" id="s_manufacturer" name="s_manufacturer">
                            <option value="" ></option>
						</select>
                    </div>               
                </div>
                <div class="form-group">
                    <label for="s_approve" class="control-label col-xs-5">Approval Status:</label>
                    <div class="col-xs-7">
                         <select class="level1 form-control col-xs-7 approve" id="s_approve" name="s_approve">
                            <option ></option>
                            <option value="1" >Needs Approval</option>
						</select>
                    </div>               
                </div>
                
            </div>
            <div class="col-xs-4">
                <div class="form-group">
                    <label for="s_status" class="control-label col-xs-5">Status:</label>
                    <div class="col-xs-7">
                         <select class="level1 form-control col-xs-7 status" id="s_status" name="s_status">
                            <option value="" ></option>
						</select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="s_lastdone_to" class="control-label col-xs-5">Last Done To:<div class="cal-cont s-form"><span class="glyphicon glyphicon-calendar"></span></div></label>
                    <div class="col-xs-7">
                        <input type="text" maxlength="100" name="s_lastdone_to" id="s_lastdone_to" class="form-control cal-field s-form" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="s_nextdue_to" class="control-label col-xs-5">Next Due To:<div class="cal-cont s-form"><span class="glyphicon glyphicon-calendar"></span></div></label>
                    <div class="col-xs-7">
                        <input type="text" maxlength="100" name="s_nextdue_to" id="s_nextdue_to" class="form-control cal-field s-form" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="s_category" class="control-label col-xs-5">Category:</label>
                    <div class="col-xs-7">
                        <select class="level1 form-control col-xs-7" id="s_category" name="s_category">
                            <option value=""></option>
                            <option value="PRIMARY">Primary</option>
                            <option value="SECONDARY">Secondary</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4"></div>
            <div class="col-lg-4">
                <div class="form-group">
                    <div class="col-lg-3"></div>
                    <div class="col-lg-6 no-pad-search" style="margin-top:0px;text-align: center">
                    	<div style="display:inline-block;">
                        	<input type="submit" name="Cust_Search_Submit" id="Cust_Search_Submit" class="btn btn-primary form-control" value="Search" />
                        </div>
						<div style="display:inline-block;">
                        	<button type="submit" name="search_reset" id="search_reset" onclick="reset_form();return false;" class="btn btn-primary form-control" style="width:80px;">Reset</button>
                         </div>
						<input type="hidden" name="Sort_Column" id="Sort_Column"  value="#VARIABLES.Sort_Column#"/>
						<input type="hidden" name="Sort_Order" id="Sort_Order"  value="#VARIABLES.Sort_Order#"/>
						<input type="hidden" name="area_filter" id="area_filter"  value="#VARIABLES.Area_Filter#"/>
                   </div>
                </div>
            </div>
        </div>
            
        <!--- Customer Search Results --->
        <div class="col-lg-12 table-responsive no-pad">
            <h4><span  id="search_results_type">#VARIABLES.lookType#</span> <i id="filter_excel_export" style="color:##1901F0;" class="fa fa-file-excel-o"></i> <i  id="filter_pdf_export"  style="color: ##F50E12; display:none;" class="fa fa-file-pdf-o"></i></h4>
            <table id="Cust_Results" class="table " style="white-space:nowrap;margin:0px;">
                <thead id="Cust_Results_Header">
                    <tr>                  
                        <th style="width:5%;"><a id="h_assetid" href="javascript:void(0);" class="sortable sort"><span class="header_title">Asset ##</span></a></th>
                        <th style="width:5%;"><a id="h_area" href="javascript:void(0);" class="sortable sort"><span class="header_title">Area</span></a></th>
                        <th style="width:5%;"><a id="h_type" href="javascript:void(0);" class="sortable sort"><span class="header_title">Type</span></a></th>
                        <th style="width:5%;"><a id="h_status" href="javascript:void(0);" class="sortable sort"><span class="header_title">Status</span></a></th>
                        <th style="width:10%;"><a id="h_manufacturer" href="javascript:void(0);" class="sortable sort"><span class="header_title">Manufacturer</span></a></th>
                        <th style="width:10%;"><a id="h_model" href="javascript:void(0);" class="sortable sort"><span class="header_title">Model</span></a></th>
                        <th style="width:10%;"><a id="h_serial" href="javascript:void(0);" class="sortable sort"><span class="header_title">Serial</span></a></th>
                        <th style="width:5%;"><a id="h_lastdone" href="javascript:void(0);" class="sortable sort"><span class="header_title">Last Done</span></a></th>
                        <th style="width:5%;"><a id="h_nextdue" href="javascript:void(0);" class="sortable sort"><span class="header_title">Next Due</span></a></th>
                        <th style="width:10%;"><a id="h_category" href="javascript:void(0);" class="sortable sort"><span class="header_title">Category</span></a></th>
                        <th style="width:5%;"><a id="h_interval" href="javascript:void(0);" class="sortable sort"><span class="header_title">Interval</span></a></th>
                        <th style="width:5%;"><a id="h_daysold" href="javascript:void(0);" class="sortable sort"><span class="header_title">Days Old</span></a></th>
                        <th style="width:10%;"><a id="h_technician" href="javascript:void(0);" class="sortable sort"><span class="header_title">Technician</span></a></th>
                        <th style="width:10%;"><a id="h_location" href="javascript:void(0);" class="sortable sort"><span class="header_title">Location</span></a></th>
                    </tr>
                </thead>
                <tbody>
                    <cfloop from="1" to="#ArrayLen(returnSelectResult.DATA.id)#" index="cust">
                    	<cfset CustomerCount = #returnSelectResult.DATA.count[cust]#>
                        <tr>
                            <td><a href="javascript:void(0);"
                            onclick="show_fai_from_filter('#returnSelectResult.DATA.assetid[cust]#');">#returnSelectResult.DATA.assetid[cust]#</a></td>
                            <td>#returnSelectResult.DATA.area[cust]#</td>
                            <td>#returnSelectResult.DATA.type[cust]#</td>
                            <td>#returnSelectResult.DATA.status[cust]#</td>
                            <td>#returnSelectResult.DATA.manufacturer[cust]#</td>
                            <td>#returnSelectResult.DATA.model[cust]#</td>
                            <td>#returnSelectResult.DATA.serial[cust]#</td>
                            <td>#DateFormat(returnSelectResult.DATA.lastdone[cust],"mm/dd/yyyy")#</td>
                            <td>#DateFormat(returnSelectResult.DATA.nextdue[cust],"mm/dd/yyyy")#</td>
                            <td>#returnSelectResult.DATA.category[cust]#</td>
                            <td style="text-align:center;">#returnSelectResult.DATA.interval[cust]#</td>
                            <td style="text-align:center;">#returnSelectResult.DATA.daysold[cust]#</td>
                            <td>#returnSelectResult.DATA.technician[cust]#</td>
                            <td style="text-align:center;">#returnSelectResult.DATA.location[cust]#</td>
                        </tr>
                    </cfloop>
                </tbody>
            </table>
           
            <div class="col-xs-12 no-pad csp-paginate-cont">
                <div class="col-xs-6 no-pad">
                    <div class="col-xs-1 no-pad">
                        <button id="CSP_Prev_Page" name="CSP_Prev_Page" class="btn btn-primary btn-left" <cfif VARIABLES.Page_Start EQ 1>disabled="disabled"</cfif>><span class="glyphicon glyphicon-arrow-left"></span></button>
                    </div>
                    <div class="col-xs-11 text-left">
                        <div class="csp-perpage">
                            <span>
                                Show
                                <select id="CSP_Per_Page" name="CSP_Per_Page">
                                    <option value="5" <cfif FORM.CSP_Per_Page EQ 5>selected="selected"</cfif>>5</option>
                                    <option value="10" <cfif FORM.CSP_Per_Page EQ 10>selected="selected"</cfif>>10</option>
                                    <option value="20" <cfif FORM.CSP_Per_Page EQ 20>selected="selected"</cfif>>20</option>
                                </select>
                                items per page
                           </span>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 no-pad">
                    <div class="col-xs-11 text-right"><div class="csp-results">Showing <span class="csp-page-start">#VARIABLES.Page_Start#</span> - <span class="csp-page-end">#VARIABLES.Page_End#</span> of (<span class="total_leads">#CustomerCount#</span>) Results</div></div>
                    <div class="col-xs-1 no-pad">
                        <button id="CSP_Next_Page" name="CSP_Next_Page" class="btn btn-primary btn-right pull-right" <cfif VARIABLES.Page_End GTE CustomerCount>disabled="disabled"</cfif>><span class="glyphicon glyphicon-arrow-right"></span></button>
                    </div>
                </div>
                <input type="hidden" id="CSP_Current_Page" name="CSP_Current_Page" value="#FORM.CSP_Current_Page#" />
            </div>
        </div>
    </form>
    
    <form name="excelExport" id="excelExport" method="post" action="./excel.cfm" target="excel_target">
		<input type="hidden" name="lead_filter" id="lead_filter" />
		<input type="hidden" name="excel_columns" id="excel_columns"/>		
		<input type="hidden" name="excel_leads_excluded" id="excel_leads_excluded"/>		
		<input type="hidden" name="show_excel_header" id="show_excel_header" value="yes"/>		
    </form> 
    <iframe id="excel_target" name="excel_target" style="display:none;"></iframe> 
<script type="text/javascript">

<!---
var json_hme_users = JSON.parse(JSON.stringify(#hme_users_json#));
var json_hme_departments = JSON.parse(JSON.stringify(#hme_departments_json#));
var json_offices_json = JSON.parse(JSON.stringify(#hme_offices_json#));

var json_dropdowns = JSON.parse(JSON.stringify(#dropDowns_json#));
--->

var assetid = "#assetid#";
var isadmin = "#SESSION.User_Admin#";
var json_dropdowns = JSON.parse(JSON.stringify(#dropDowns_json#));
var json = JSON.parse(JSON.stringify(#leads_json#));
var leadFilter = JSON.stringify(#leadFilter_json#);
var lead_count = #CustomerCount#;

console.log(json_dropdowns);
console.log(assetid);
console.log(isadmin);
</script>
</cfoutput>
<cfinclude template="./_tmp_search_form.cfm">

