<cfinclude template="./serverSettings.cfm">
<cfsetting showdebugoutput="no">
<cfparam name="FORM.faiID" default="">
<cfparam name="FORM.fai_report_req" default="">
<cfparam name="FORM.fai_report_from_date" default="">
<cfparam name="FORM.fai_report_to_date" default="">
<cfparam name="FORM.fai_report_storedProcedure" default="">
<cfparam name="FORM.fai_report_sortorder" default="">
<cfparam name="FORM.fai_report_sortColumn" default="">
<cfparam name="FORM.fai_report_custom" default="">
<cfparam name="FORM.fai_filters" default="">

<cfscript>	
	obj = CreateObject("component", "_cfcs.calibration");
	jsonFAI = StructNew();
	switch(FORM.fai_report_req){
	case 'fai_traveler':
	case 'completion_notification_report':
		jsonFAI["id"] = FORM.faiID;
		fai = obj.searchByFAINo(argumentCollection = jsonFAI);
		break;
	case 'export_filter_excel':
	case 'export_filter_pdf':
		fai = obj.custom_report(argumentCollection = DeserializeJSON(FORM.fai_filters));
      	tempPath  =   GetTempDirectory() & CreateUuid() & ".xls";
        spreadsheetNewFromQuery(data=fai,path=tempPath);
        fileName = "Calibration_Report_" & DateFormat(Now(), "mmddyy") & TimeFormat(Now(), "_HHmmss") & ".xls";   
		break;
	default:
		jsonFAI["custom"] = FORM.fai_report_custom;
		jsonFAI['pageNumber'] = '1';
		jsonFAI['itemsPerPage'] = '5000';
		fai  = obj.custom_report(argumentCollection = jsonFAI);
		break;
	}
</cfscript>
<cfset logoImage ='./images/hme-companies.png'>
<cfset tmp_file_name = "#GetTempDirectory()#/#CreateUUID()#.pdf">

<cfif FORM.fai_report_req eq 'export_filter_excel'>
  <cfheader name="Content-Disposition" value="attachment; filename=#fileName#">
    <cfcontent  
        type = "application/msexcel"  
        file = "#tempPath#"  
        deleteFile = "Yes">
</cfif> 

<cfif FORM.fai_report_req eq 'completion_notification_report'>
    <cfinclude template="_dat_fai_completion_notification_report.cfm">
    <cfheader name="Content-Disposition" value="attachment; filename=FAI #fai.id# Completion Notification.pdf">
    <cfcontent reset="Yes" type="application/pdf" file="#tmp_file_name#" deletefile="Yes">  
</cfif> 

<cfif FORM.fai_report_req eq 'fai_traveler'>
    <cfinclude template="_dat_fai_traveler.cfm">
    <cfheader name="Content-Disposition" value="attachment; filename=FAI #fai.id# fai travler.pdf">
    <cfcontent reset="Yes" type="application/pdf" file="#tmp_file_name#" deletefile="Yes">  
</cfif> 

<cfif FORM.fai_report_req eq 'closed_the_day_before_report'>
	<cfset fromdate = FORM.fai_report_from_date> 
	<cfset todate = FORM.fai_report_to_date> 
    <cfinclude template="_dat_closed_the_day_before_report.cfm">
    <cfheader name="Content-Disposition" value="attachment; filename=FAI_closed_the_day_before_report.pdf">
    <cfcontent reset="Yes" type="application/pdf" file="#tmp_file_name#" deletefile="Yes">  
</cfif>
 
<cfif FORM.fai_report_req eq 'work_in_progress_report'>
    <cfinclude template="_dat_wip_report.cfm">
    <cfheader name="Content-Disposition" value="attachment; filename=FAI_work_in_progress_report.pdf">
    <cfcontent reset="Yes" type="application/pdf" file="#tmp_file_name#" deletefile="Yes">  
</cfif> 
 
<cfif FORM.fai_report_req eq 'completed_by_ri_date_report'>
    <cfinclude template="_dat_completed_by_ri_date_report.cfm">
	<cfheader name="Content-Disposition" value="attachment;filename=FAI #fai.id# Completion Notification.pdf">
    <cfcontent reset="Yes" type="application/pdf" file="#tmp_file_name#" deletefile="Yes">  
</cfif> 

 
<cffunction name="spreadsheetNewFromQuery" output="false">
    <cfargument name="data" type="query" required="true">
    <cfargument name="path" type="string" required="true">
    <cfargument name="sheetName" type="string" default="Sheet1">
    <cfargument name="removeHeaderRow" type="boolean" default="false">
    <cfspreadsheet action="write" filename="#path#" query="data" sheetname="#sheetName#" overwrite="true">  
</cffunction>
    
<!---
<cfheader name="Content-Disposition" value="inline; filename=FAIcompleted_by_ri_date_report.pdf">

<cfheader name="Content-Disposition" value="attachment;filename=FAI #fai.id# Completion Notification.pdf">
<cfcontent type="application/octet-stream" file="#GetTempDirectory()#/FAI_#fai.id#_Completion_Notification.pdf" deletefile="Yes">
--->

