<cfparam name="FORM.CSP_Page_Form" default="">
<cfparam name="FORM.CSP_Per_Page" default="20">
<cfparam name="FORM.CSP_Current_Page" default="1">
<cfparam name="VARIABLES.Page_Start" default="1">
<cfparam name="VARIABLES.Page_End" default="5">
<cfparam name="VARIABLES.Sort_Column" default="[nextdue]">
<cfparam name="VARIABLES.Sort_Order" default="desc">
<cfparam name="VARIABLES.lookType" default="Search Results">
<cfparam name="VARIABLES.Area_Filter" default="">

<cfparam name="FORM.Lead_Name" default="">
<cfparam name="FORM.Lead_City" default="">
<cfparam name="FORM.Lead_State" default="">
<cfparam name="FORM.Lead_Zip" default="">
<cfparam name="FORM.Lead_Email" default="">
<cfparam name="FORM.Lead_Company" default="">
<cfparam name="FORM.Lead_StoreNumber" default="">
<cfparam name="FORM.Lead_Interest" default="">
<cfparam name="FORM.Lead_Phone" default="">
<cfparam name="FORM.Lead_Type" default="">
<cfparam name="FORM.Lead_Date_To" default="">
<cfparam name="FORM.Lead_Date_From" default="">

<cfparam name="FORM.Sort_Column" default="h_nextdue">
<cfparam name="FORM.Sort_Order" default="desc">
<cfparam name="FORM.Area_Filter" default="">

<cfscript>
	obj = CreateObject("component", "_cfcs.calibration");
	serializer = new lib.JsonSerializer();

	CustomerCount = 0;
	leads_json = '';
	dropDowns_json = {};

	faiFilter = StructNew();
	faiFilter['storedProcedure'] = 'dbo.[get_Manufacturer]';
	Manufacturers = obj.DropDowns(argumentCollection = faiFilter);

	faiFilter['storedProcedure'] = 'dbo.[get_Intervals]';
	Intervals = obj.DropDowns(argumentCollection = faiFilter);

	faiFilter['storedProcedure'] = 'dbo.[get_Status]';
	Status = obj.DropDowns(argumentCollection = faiFilter);

	faiFilter['storedProcedure'] = 'dbo.[get_Areas]';
	Areas = obj.DropDowns(argumentCollection = faiFilter);
	
	faiFilter['storedProcedure'] = '';
	faiFilter['sortColumn'] = '';
	faiFilter['pageNumber'] = FORM.CSP_Current_Page;
	faiFilter['itemsPerPage'] = FORM.CSP_Per_Page;
	faiFilter['custom'] = 'default';
	returnSelect = obj.custom(argumentCollection = faiFilter);
	leadFilter_json = serializer.serialize(StructCopy(faiFilter));
    leads_json = serializer.serialize(returnSelect);
	returnSelectResult = DeserializeJSON(SerializeJSON(returnSelect, 'true'));

	dropDowns = StructNew();	
	dropDowns['manufacturers'] = Manufacturers;
	dropDowns['intervals'] = Intervals;
	dropDowns['areas'] = Areas;
	dropDowns['status'] = Status;
    dropDowns_json = serializer.serialize(StructCopy(dropDowns));
 	StructClear(dropDowns);	

 	StructClear(faiFilter);	
 		
	VARIABLES.Page_Start = ((FORM.CSP_Current_Page-1)*FORM.CSP_Per_Page)+1;
	VARIABLES.Page_End = FORM.CSP_Current_Page*FORM.CSP_Per_Page;
	VARIABLES.Sort_Column = FORM.Sort_Column;
	VARIABLES.Sort_Order = FORM.Sort_Order;	
	VARIABLES.lookType = "Search Results (Current Month)";
	VARIABLES.Area_Filter = SESSION.User_Area_Filter;
	fai_security = "0";

	
</cfscript>



