USE [db_calibration]
GO
/****** Object:  StoredProcedure [dbo].[custom_select]    Script Date: 4/15/2019 10:45:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[custom_select](
	@pageNumber INT = NULL,
	@itemsPerPage INT = NULL,
	@assetid NVARCHAR(50) = NULL,
	@lastdone_from NVARCHAR(50) = NULL,
	@lastdone_to NVARCHAR(50) = NULL,
	@nextdue_from NVARCHAR(50) = NULL,
	@nextdue_to NVARCHAR(50) = NULL,
	@area NVARCHAR(255) = NULL,
	@type NVARCHAR(50) = NULL,
	@status NVARCHAR(255) = NULL,
	@model NVARCHAR(50) = NULL,
	@interval NVARCHAR(50) = NULL,
	@serial NVARCHAR(50) = NULL,
	@testprocedure NVARCHAR(50) = NULL,
	@technician NVARCHAR(50) = NULL,
	@location NVARCHAR(50) = NULL,
	@notes NVARCHAR(50) = NULL,
	@category NVARCHAR(255) = NULL,
	@manufacturer NVARCHAR(255) = NULL,
	@ontimestatus NVARCHAR(255) = NULL,
	@custom NVARCHAR(50) = NULL,
	@sortcolumn NVARCHAR(50) = NULL,
	@sortorder NVARCHAR(5) = NULL,
	@area_filter NVARCHAR(255) = NULL

) AS

DECLARE
	@rowCount   NVARCHAR(100),
	@pageFetch  NVARCHAR(300),
	@cnt INT,
	@recordSet INT,
	@currentDate DateTime,
	@culture nvarchar(10)

SET @culture = 'en-US'
SET @currentDate = GETDATE()

IF @area_filter IS NOT NULL
BEGIN
	SET @area = @area_filter
END

IF @pageNumber IS NOT NULL AND  @itemsPerPage IS NOT NULL
	BEGIN
		SET @recordSet = (@pageNumber * @itemsPerPage) - @itemsPerPage
		SET @pageFetch = ' OFFSET ' + CAST(@recordSet as NVARCHAR(5)) + ' ROWS FETCH NEXT ' + CAST(@itemsPerPage as NVARCHAR(5))  + ' ROWS ONLY'
	END

SELECT @cnt = count(*) 
FROM HMECAL
WHERE 1=1
 AND (@custom  != 'assetidLookup'  OR ([Asset/ ID] = @assetid))
 AND (@custom  != 'default'  OR  DATEDIFF(d, [nextdue] , GETDATE())>0   --[nextdue] >= DATEADD(mm, DATEDIFF(mm, 0, GETDATE()), 0)
	--AND [nextdue] <= DATEADD (dd, -1, DATEADD(mm, DATEDIFF(mm, 0, GETDATE()) + 1, 0))
	AND [status] =  'ACTIVE'
	--AND category = 'SECONDARY'
	)
 AND (@custom != 'search' OR (@lastdone_from IS NULL OR  [lastdone] >= @lastdone_from)
	AND (@lastdone_to IS NULL OR [lastdone] <= @lastdone_to)
	AND (@nextdue_from IS NULL OR  [nextdue] >= @nextdue_from)
	AND (@nextdue_to IS NULL OR [nextdue] <= @nextdue_to)
	AND (@assetid IS NULL OR [Asset/ ID] LIKE '%' + @assetid + '%' )
	--AND (@area IS NULL OR [area] LIKE '%' + @area + '%' )
	--AND (@area IS NULL OR [area] = @area )
	AND (@type IS NULL OR [type] LIKE '%' + @type + '%' )
	AND (@status IS NULL OR [status] = @status )
	AND (@model IS NULL OR [model]  LIKE '%' + @model + '%')
	AND (@interval IS NULL OR [interval] LIKE '%' + @interval + '%')
	AND (@serial IS NULL OR [serial] LIKE '%' + @serial + '%')
	AND (@testprocedure IS NULL OR [test procedure] LIKE '%' + @testprocedure + '%')
	AND (@technician IS NULL OR [technician] LIKE '%' + @technician + '%')
	AND (@location IS NULL OR [location] LIKE '%' + @location + '%')
	AND (@notes IS NULL OR [notes] LIKE '%' + @notes + '%')
	AND (@category IS NULL OR [category] LIKE '%' + @category + '%')
	AND (@manufacturer IS NULL OR [manufacturer] LIKE '%' + @manufacturer + '%')
	AND (@ontimestatus IS NULL OR [on time status] LIKE '%'+ @ontimestatus + '%'))

	AND (@area IS NULL OR [area] = @area )

SELECT 
	@cnt as count
	,dbo.inv([Asset/ ID]) AS assetid
	,dbo.inv([area]) AS area
	,dbo.inv([type]) AS type
	,dbo.inv([status]) AS status
	,dbo.inv([model]) AS model
	,dbo.inv(FORMAT([lastdone],'d', @culture)) AS lastdone
	,dbo.inv(FORMAT([nextdue],'d', @culture)) AS nextdue
	,dbo.inv([interval]) AS interval
	,dbo.inv([serial]) AS serial
	,dbo.inv([test procedure]) AS testprocedure
	,dbo.inv([technician]) AS technician
	,dbo.inv([location]) AS location
	,dbo.inv([notes]) AS notes
	,dbo.inv([category]) AS category
	,dbo.inv([manufacturer]) AS manufacturer
	,dbo.inv([on time status]) AS ontimestatus
	,[id] AS id	
	,DATEDIFF(d, [nextdue] , GETDATE()) AS daysold
	,[approve] AS approve	

FROM [dbo].HMECAL calibration
WHERE 1=1
 AND (@custom  != 'assetidLookup'  OR ([Asset/ ID] = @assetid))
 AND (@custom  != 'default'  OR  DATEDIFF(d, [nextdue] , GETDATE())>0   --[nextdue] >= DATEADD(mm, DATEDIFF(mm, 0, GETDATE()), 0)
	--AND [nextdue] <= DATEADD (dd, -1, DATEADD(mm, DATEDIFF(mm, 0, GETDATE()) + 1, 0))
	AND [status] =  'ACTIVE'
	--AND category = 'SECONDARY'
	)
 AND (@custom != 'search' OR (@lastdone_from IS NULL OR  [lastdone] >= @lastdone_from)
	AND (@lastdone_to IS NULL OR [lastdone] <= @lastdone_to)
	AND (@nextdue_from IS NULL OR  [nextdue] >= @nextdue_from)
	AND (@nextdue_to IS NULL OR [nextdue] <= @nextdue_to)
	AND (@assetid IS NULL OR [Asset/ ID] LIKE '%' + @assetid + '%' )
	--AND (@area IS NULL OR [area] LIKE '%' + @area + '%' )
	--AND (@area IS NULL OR [area] = @area )
	AND (@type IS NULL OR [type] LIKE '%' + @type + '%' )
	AND (@status IS NULL OR [status] = @status)
	AND (@model IS NULL OR [model]  LIKE '%' + @model + '%')
	AND (@interval IS NULL OR [interval] LIKE '%' + @interval + '%')
	AND (@serial IS NULL OR [serial] LIKE '%' + @serial + '%')
	AND (@testprocedure IS NULL OR [test procedure] LIKE '%' + @testprocedure + '%')
	AND (@technician IS NULL OR [technician] LIKE '%' + @technician + '%')
	AND (@location IS NULL OR [location] LIKE '%' + @location + '%')
	AND (@notes IS NULL OR [notes] LIKE '%' + @notes + '%')
	AND (@category IS NULL OR [category] LIKE '%' + @category + '%')
	AND (@manufacturer IS NULL OR [manufacturer] LIKE '%' + @manufacturer + '%')
	AND (@ontimestatus IS NULL OR [on time status] LIKE '%'+ @ontimestatus + '%'))

	AND (@area IS NULL OR [area] = @area )

ORDER BY 
		 (case when @sortcolumn = '[Asset/ ID]' and @sortorder = 'asc' then [Asset/ ID] end)  asc,
         (case when @sortcolumn = '[Asset/ ID]' and @sortorder = 'desc' then [Asset/ ID] end) desc,
         (case when @sortcolumn = '[area]' and @sortorder = 'asc' then [area] end) asc,
         (case when @sortcolumn = '[area]' and @sortorder = 'desc' then [area] end) desc,
         (case when @sortcolumn = 'type]' and @sortorder = 'asc' then [type] end) asc,
         (case when @sortcolumn = '[type]' and @sortorder = 'desc' then [type] end) desc,
         (case when @sortcolumn = '[status]' and @sortorder = 'asc' then [status] end) asc,
         (case when @sortcolumn = '[status]' and @sortorder = 'desc' then [status] end) desc,
         (case when @sortcolumn = '[model]' and @sortorder = 'asc' then [model] end) asc,
         (case when @sortcolumn = '[model]' and @sortorder = 'desc' then [model] end) desc,
         (case when @sortcolumn = '[manufacturer]' and @sortorder = 'asc' then [manufacturer] end) asc,
         (case when @sortcolumn = '[manufacturer]' and @sortorder = 'desc' then [manufacturer] end) desc,
         (case when @sortcolumn = '[lastdone]' and @sortorder = 'asc' then [lastdone] end) asc,
         (case when @sortcolumn = '[lastdone]' and @sortorder = 'desc' then [lastdone] end) desc,
         (case when @sortcolumn = '[nextdue]' and @sortorder = 'asc' then [nextdue] end) asc,
         (case when @sortcolumn = '[nextdue]' and @sortorder = 'desc' then [nextdue] end) desc,
         (case when @sortcolumn = '[test procedure]' and @sortorder = 'asc' then [test procedure] end) asc,
         (case when @sortcolumn = '[test procedure]' and @sortorder = 'desc' then [test procedure] end) desc,

		 (case when @sortcolumn = '[interval]' and @sortorder = 'asc' then [interval] end) asc,
         (case when @sortcolumn = '[interval]' and @sortorder = 'desc' then [interval] end) desc,
         (case when @sortcolumn = '[location]' and @sortorder = 'asc' then [location] end) asc,
         (case when @sortcolumn = '[location]' and @sortorder = 'desc' then [location] end) desc,
         (case when @sortcolumn = '[technician]' and @sortorder = 'asc' then [technician] end) asc,
         (case when @sortcolumn = '[technician]' and @sortorder = 'desc' then [technician] end) desc,
         (case when @sortcolumn = '[category]' and @sortorder = 'asc' then [category] end) asc,
         (case when @sortcolumn = '[category]' and @sortorder = 'desc' then [category] end) desc,
         (case when @sortcolumn = '[serial]' and @sortorder = 'asc' then [serial] end) asc,
         (case when @sortcolumn = '[serial]' and @sortorder = 'desc' then [serial] end) desc,

		 (case when @sortcolumn = '[daysold]' and @sortorder = 'asc' then DATEDIFF(d, [nextdue] , GETDATE()) end) asc,
         (case when @sortcolumn = '[daysold]' and @sortorder = 'desc' then DATEDIFF(d, [nextdue] , GETDATE())end) desc,

		 --(case when @custom = 'ewip' then getdate()-[DATE RECEIVED] end) desc,
		 --(case when @custom = 'rc' then getdate()-[DATE RECEIVED] end) desc,
		 --(case when @custom = 'rinc' then getdate()-[DATE RECEIVED] end) desc,	
		 --(case when @custom = 'aqe' then getdate()-[DATE RECEIVED] end) desc,	
		 --(case when @custom = 'riic' then getdate()-[DATE RECEIVED] end) desc,
		 --(case when @custom = 'cbrid' then getdate()-[DATE RECEIVED] end) desc,
		 --(case when @custom = 'cdb' then getdate()-[DATE RECEIVED] end) desc,
		 --(case when @custom = 'wipnonpcb' then getdate()-[DATE RECEIVED] end) desc,
		 --(case when @custom = 'wippcb' then getdate()-[DATE RECEIVED] end) desc,
		 --(case when @custom = 'search' then getdate()-[DATE RECEIVED] end) desc,
		 
         (case when @sortcolumn IS NULL then  [nextdue] end) desc

OFFSET @recordSet ROWS FETCH NEXT @itemsPerPage ROWS ONLY
