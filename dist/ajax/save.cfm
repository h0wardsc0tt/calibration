<cfsetting showdebugoutput="no">
<cfset assetid = "">
<cfscript>		
	if (#FindNoCase('application/json', cgi.content_type)# > 0)
	{
    	jsonFAI = deserializeJSON(ToString(getHTTPRequestData().content));	
    	assetid = jsonFAI['assetid'];
		obj = CreateObject("component", "_cfcs.calibration");
		returnSelect = obj.update(argumentCollection = jsonFAI);
		StructClear(jsonFAI);		
	}
	else
	{
		returnSelect = StructNew();
		returnSelect['results'] = "No";
	}
	serializer = new lib.JsonSerializer();
	writeOutput(serializer.serialize(returnSelect));
	StructClear(returnSelect);		
</cfscript>
<cftry>
    <cfmail to="mhoward@hme.com,dday@hme.com" cc="hscott@hme.com" from="no-reply@hme.com" subject="Calabration Asset updated: #assetid#" type="html">      
       <p> 
             <img src="cid:logo" alt="" /><br /> 
             <span style="font-size:15px; color:##CCC8C8;">TEST AND MEASUREMENT EQUIPMENT CALIBRATION</span><br>
       </p> 
    
		Calabration Asset updated:  <a href="#APPLICATION.rootURL#/?asset=#assetid#">#assetid#</a><br>
		Update By : #SESSION.User_Name#<br>
		Department : #SESSION.User_Dept#<br>
		Location : #SESSION.User_LOCATION#
        <cfmailparam  
             file="#ExpandPath('/images/hme-companies.png')#" 
             contentid="logo"  
             disposition="inline" 
        /> 
    </cfmail>
    <cfcatch type="Application">
    	<cfset results = #cfcatch.detail#>
    	<!---<cflog text="#cfcatch.detail#" file="mail" type="Error" application="yes">--->
    </cfcatch>
</cftry>
