<cfsetting showdebugoutput="no">
<cfscript>		
	if (#FindNoCase('application/json', cgi.content_type)# > 0)
	{
    	jsonFAI = deserializeJSON(ToString(getHTTPRequestData().content));	
		obj = CreateObject("component", "_cfcs.calibration");
		returnSelect = obj.Admin(argumentCollection = jsonFAI);
		StructClear(jsonFAI);		
	}
	else
	{
		returnSelect = StructNew();
	}
	serializer = new lib.JsonSerializer();
	writeOutput(serializer.serialize(returnSelect));
	StructClear(returnSelect);		
</cfscript>

