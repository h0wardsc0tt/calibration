var uploadObj;
var fai_Id = '';
var json_fai = '';
var isFAI = false;
var default_sortorder = 'desc';
var default_sortcolumn = '[DATE COMPLETED]';
var fileUploadError = false;
var changed_flag = false;
var follow_through = false;

$(document).ready(function() {
	$.ajax({
		type: "POST",
		contentType: "application/json; charset=utf-8",
		url: "ajax/fai_directory.cfm",
		success: function (data) {	
			var directory = JSON.parse(data);				
			console.log(directory);
			json_hme_users =JSON.parse(directory.users);
			build_address_book(json_hme_users, 'givenname');	
			json_save_address_book = contacts;	

		},
		error: function (jqXHR, ajaxOptions, thrownError) {
			$('#login_iframe').attr('src', 'login.cfm');
			$('#login_modal').modal('show');
		}	   
	});	

	$('form input[type=text]').change(function(e) {
		$(this).attr('changed','1');
		changed_flag = true;
		console.log(this)
	});
	$('form textarea').change(function(e) {
		$(this).attr('changed','1');
		changed_flag = true;
		console.log(this)
	});
	$('form select').change(function(e) {
		$(this).attr('changed','1');
		changed_flag = true;
		console.log(this)
	});
	$('form input[type=checkbox]').change(function(e) {
		$(this).attr('changed','1');
		console.log(this)
	});

	$('.expandable-element').height(0);
	$('#CSP_Per_Page').show();

	$('.row .fai-cols').addClass('col-xs-6');
	$('.row .fai-cols .control-label').addClass('col-xs-5');
	$('.row .fai-cols .form-group div').addClass('col-xs-7');

					

	$(".input-date" ).datepicker({});

	$('.modal-dialog').draggable({
		handle: ".modal-header"
	});

	$('#email_form').draggable({
		handle: "#email_header, .email-footer-buttons"
	});


	$( ".cal-field" ).click(function(e) {
		$(this).datepicker('setDate', null);
	});

	$( ".cal-field" ).datepicker({
		onSelect: function(){}	
	});

	$(".sortable" ).click(function(e) {
		sort_results($(this));
	});

	$('.header_title').removeClass('sorted');

	var sortedColumn = $('#' + $('#Sort_Column').val());
	var sortedColumnId = sortedColumn.prop('id');
	
	$('#' + sortedColumnId + ' span:first-child').addClass('sorted');
	sortedColumn.removeClass('sort').addClass($('#Sort_Order').val());	

	// Paginate to Next page
	$('#CSP_Next_Page').click(function(e) {
		console.log('#CSP_Next_Page');
		lead_paging(1);
		return false;
	});

	// Paginate to Previous page
	$('#CSP_Prev_Page').click(function(e) {
		lead_paging(-1);
		return false;
	});	
	// Toggle items per page
	$('#CSP_Per_Page').change(function(e) {
		$('#CSP_Current_Page').val(1);
		lead_paging(0);
		return false;
	});

	$('#filter_excel_export').click(function(e) {
		export_filter('excel');
	});	

	$('#filter_pdf_export').click(function(e) {
		export_filter('pdf');
	});	

	$('.numeric').keypress(function(e){	
		var keyCode = (e.keyCode ? e.keyCode : e.which);
		if(keyCode == 8 || keyCode == 9) return true;					
		if(!$.isNumeric(String.fromCharCode(keyCode))){return false;}
	});	

	$('.fai-lookup').keypress(function(e){	
		var keyCode = (e.keyCode ? e.keyCode : e.which);
		if(keyCode == 13 || keyCode == 8 || keyCode == 9){return true;}					
		//if(!$.isNumeric(String.fromCharCode(keyCode))){return false;}
	});	

	$('.noEnterKey').keypress(function(e){	
		var keyCode = (e.keyCode ? e.keyCode : e.which);
		if(keyCode == 13){return false;}					
	});	

	buildDropDowns();
	
	console.log(json);
	console.log(assetid);
	if(assetid != ''){
		setTimeout(function(){
			$('#Search').val(assetid)
			asset_Number_Lookup();
		}, 10);
	}
});

function buildDropDowns(){
	var obj = json_dropdowns.manufacturers;
	var list = $(".manufacturer");

	$.each(obj, function(index, item) {
	  list.append(new Option(item.manufacturer, item.manufacturer));
	});	
	
	obj = json_dropdowns.intervals;
	list = $(".interval");

	$.each(obj, function(index, item) {
	  list.append(new Option(item.interval, item.interval));
	});	
	
	obj = json_dropdowns.areas;
	list = $(".area");
	
	var area_filter = $('#area_filter').val();
	console.log(area_filter)
	if(area_filter == ''){
		$.each(obj, function(index, item) {
		  list.append(new Option(item.area, item.area));
		});	
	}else{
	  	list.append(new Option(area_filter, area_filter));
	}

	obj = json_dropdowns.status;
	list = $(".status");

	$.each(obj, function(index, item) {
	  list.append(new Option(item.status, item.status));
	});	

	
}
