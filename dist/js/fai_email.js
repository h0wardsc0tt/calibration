var regExp_email = new RegExp(/[\w!#$%&'*+-/=?^_`{|}~ ]+@([\w!#$%&'*+-/=?^_`{|}~ ]+\.)+[\w!#$%&'*+-/=?^_`{|}~ ]+/);
function send_message(){
	json_email_resp.emailcontent = $('.nicEdit-main').html();
	if($('.email-to').selectpicker('val') != null)
		json_email_resp.to = $('.email-to').selectpicker('val').join(';');
	else
		json_email_resp.to = '';
	if($('.email-cc').selectpicker('val') != null)
		json_email_resp.cc = $('.email-cc').selectpicker('val').join(';');
	else
		json_email_resp.cc = '';
	if($('.email-attachments').selectpicker('val') != null)
		json_email_resp.attachments = $('.email-attachments').selectpicker('val').join(';');
	else
		json_email_resp.attachments = '';
	json_email_resp.subject = $('.email-subject').val();
	send_json_email(json_email_resp);
}

var email_spinner;
function send_json_email(data){
	if($('.email-send-button').hasClass('email_send_btn_active')) return;
	console.log(data);
	$('.email-send-button').addClass('email_send_btn_active');
	email_spinner = document.getElementById('email_form');
	var opts = spinnerOptions();
	email_spinner = new Spinner(opts).spin(email_spinner);
	$.ajax({
		type: "POST",
		contentType: "application/json; charset=utf-8",
		url: "ajax/send_email.cfm",
		data: JSON.stringify(data),
		dataType: "json",
		success: function (data) {					
			email_spinner.stop();
			$('.email-send-button').removeClass('email_send_btn_active');
			if (data != null) {
				console.log(data);
				if(data.results == "OK"){
					$('.email-element').hide();
					display_status_message('email sent');
				}
				else{
					display_email_validation(data.results);
				}
			}
			else{
			}
		},
		error: function (jqXHR, ajaxOptions, thrownError) {
			$('.email-send-button').removeClass('email_send_btn_active');
			email_spinner.stop();
			$('#login_iframe').attr('src', 'login.cfm');
			$('#login_modal').modal('show');
			//setTimeout(function(){location.href=location.href;}, 300);	   
        }
	});	
}

function display_email_validation(msg){
	$('.email-validatrion-error .error-message').html(msg);
	$('.email-validatrion-error .expandable-element').height($('.email-validatrion-error .error-message').prop('scrollHeight')+2);
	$('.email-validatrion-error .email_close_btn').show();
}

function close_email_validation(){
	$('.email-validatrion-error .expandable-element').height(0);
	$('.email-validatrion-error .email_close_btn').hide();
}

function email_click_through(obj){
	obj.hide();
}