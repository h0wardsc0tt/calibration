
<cfoutput>
<div id="fai_search" class="container-fluid" style="min-height: 100%;position:relative;">
<cfinclude template="./_dat_calibration_report_prep.cfm">
<cfinclude template="./_tmp_calibration_lookup.cfm">
</div>

<div id="fai_report" class="container-fluid hidden" style="min-height: 100%;position:relative;">
	<div class="blockout_form"></div>
	<div class="fai-header" align="center">TEST AND MEASUREMENT EQUIPMENT CALIBRATION - DATA ENTRY SCREEN</div>
    <form action="" method="POST" id="fai_form" class="form-horizontal col-lg-12" role="form" accept-charset="UTF-8">
        <div class="row">
            <div class="form-col fai-header-cols col-xs-3">
               <div class="form-group">
                   <label for="assetid" class="control-label col-xs-2">ASSET:</label>
                    <div class="fai-input col-xs-7">
                        <input id="assetid" name="assetid" type="text" style="width:90px; text-align:center;">
                    </div>
                </div>
            </div>
            <div class="form-col fai-header-cols col-xs-4">
                <div class="form-group">
                   <label for="status" class="control-label col-xs-5">STATUS:</label>
                    <div class="fai-input">
                        <select class="level1 form-control col-xs-7 status" id="status" name="status" style="width: 150px;">
                            <option value="" disabled selected>Select Status</option>
                            <option value="" ></option>
						</select>
                    </div>
                </div>
            </div>
           <div class="form-col fai-header-cols col-xs-5">
                <div class="form-group">
                   <label for="calibrationperformed" class="control-label col-xs-6">CALIBRATED:</label>
                    <div class="fai-input col-xs-6">
                        <input id="calibrationperformed" name="calibrationperformed" type="checkbox" style="width: 20px;height: 20px;">
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="padding-bottom:3px;">
            <div class="form-col receitp-info col-xs-12">
            CALIBRATION INFORMATION
            </div>
        </div>
    	<div class="row" style="position:relative;">
        <div class="blockout_level_1 blockout_section"></div>
            <div class="form-col fai-cols">
                <div class="form-group">
                    <label for="category" class="control-label fai-label-color">CATEGORY :</label>
                    <div class="fai-input">
                        <select class="level1 form-control col-xs-7" id="category" name="category">
                            <option value="" disabled selected>Select Category</option>
                            <option value="PRIMARY">Primary</option>
                            <option value="SECONDARY">Secondary</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="nextdue" class="control-label fai-label-color">NEXT DUE :</label>
                    <div class="fai-input">
                        <input type="text" readonly maxlength="100" name="nextdue" id="nextdue" class="level1 form-control fai-input-color"/>
                    </div>
                </div>
				<div class="form-group">
                    <label for="lastdone" class="control-label fai-label-color"><span onclick="lastdoneToday();" class="fa fa-calendar" onmouseover="this.style.cursor='pointer';">&nbsp;</span>LAST DONE :</label>
                    <div class="fai-input">
                        <input type="text" maxlength="100" name="lastdone" id="lastdone" class="level1 form-control fai-input-color input-date"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="area" class="control-label fai-label-color">AREA :</label>
                    <div class="fai-input">
                         <select class="level1 form-control col-xs-7 area" id="area" name="area">
                            <option value="" disabled selected>Select Area</option>
                            <option value="" ></option>
						</select>
                     </div>
                </div>
                <div class="form-group">
                    <label for="manufacturer" class="control-label fai-label-color">MANUFACTURER :</label>
                    <div class="fai-input">
                        <select class="level1 form-control col-xs-7 manufacturer" id="manufacturer" name="manufacturer">
                            <option value="" disabled selected>Select Manufacturer</option>
                            <option value="" ></option>
						</select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="type" class="control-label fai-label-color">TYPE :</label>
                    <div class="fai-input">
                        <input type="text" maxlength="100" name="type" id="type" class="level1 form-control fai-input-color noEnterKey"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="type" class="control-label fai-label-color">MODEL :</label>
                    <div class="fai-input">
                        <input type="text" maxlength="100" name="model" id="model" class="level1 form-control fai-input-color noEnterKey"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="serial" class="control-label fai-label-color">SERIAL NUMBER :</label>
                    <div class="fai-input">
                        <input type="text" maxlength="100" name="serial" id="serial" class="level1 form-control fai-input-color noEnterKey"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="testprocedure" class="control-label fai-label-color">TEST PROCEDURE :</label>
                    <div class="fai-input">
                        <input type="text" maxlength="100" name="testprocedure" id="testprocedure" class="level1 form-control fai-input-color noEnterKey"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="po" class="control-label fai-label-color">LOCATION :</label>
                    <div class="fai-input">
                        <input type="text" maxlength="100" name="location" id="location" class="level1 form-control fai-input-color noEnterKey"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="interval" class="control-label fai-label-color">INTERVAL :</label>
                    <div class="fai-input">
                        <select class="level1 form-control col-xs-7 interval" id="interval" name="interval">
                            <option value="" disabled selected>Select Interval</option>
                            <option value="" ></option>
						</select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="technician" class="control-label fai-label-color">TECHNICIAN :</label>
                    <div class="fai-input">
                         <input type="text" maxlength="100" name="technician" id="technician" class="level1 form-control fai-input-color noEnterKey"/>
                    </div>
                </div>

                
            </div>

            <div class="form-col fai-right-cols  col-xs-6">
                <div class="form-group" style="margin-bottom: 0px;">
                	<div align="center" style="text-align:center;background-color:##7f7f7f; color:##fff;">NOTES</div>               	           
                </div>
                <div class="form-group form-group-notes" style="margin-bottom:0px;">
                	<textarea id="notes" class="level1-textarea notes fai-input-color" name="notes" style="overflow-y: auto"> 
                    </textarea>
                    <input type="text" maxlength="100" name="user" id="user" value="#SESSION.User_Name#" style="display:none;"/>  
                             
                </div>
            </div>
        </div>

	</form>
</div>
<cfinclude template="./_tmp_email_form.cfm">
</cfoutput>




