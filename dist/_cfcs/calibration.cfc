<cfcomponent> 

    <cffunction name="custom" output="false" returntype="query" access="public">
        <cfargument type="any" name="pageNumber" dbvarname="@pageNumber" required="no">
        <cfargument type="any" name="itemsPerPage" dbvarname="@itemsPerPage" required="no">
        <cfargument type="string" name="lastdone_from" dbvarname="@lastdone_from" required="no">
        <cfargument type="string" name="lastdone_to" dbvarname="@lastdone_to" required="no">
        <cfargument type="string" name="nextdue_from" dbvarname="@nextdue_from" required="no">
        <cfargument type="string" name="nextdue_to" dbvarname="@nextdue_to" required="no">        
		<cfargument type="string" name="assetid" dbvarname="@assetid" required="no">
		<cfargument type="string" name="area" dbvarname="@area" required="no">
		<cfargument type="string" name="type" dbvarname="@type" required="no">
		<cfargument type="string" name="status" dbvarname="@status" required="no">
		<cfargument type="string" name="model" dbvarname="@model" required="no">
		<cfargument type="string" name="interval" dbvarname="@interval" required="no">
		<cfargument type="string" name="serial" dbvarname="@serial" required="no">
		<cfargument type="string" name="testprocedure" dbvarname="@testprocedure" required="no">
		<cfargument type="string" name="technician" dbvarname="@technician" required="no">
		<cfargument type="string" name="location" dbvarname="@location" required="no">
		<cfargument type="string" name="notes" dbvarname="@notes" required="no">
		<cfargument type="string" name="category" dbvarname="@category" required="no">
		<cfargument type="string" name="manufacturer" dbvarname="@manufacturer" required="no">        
		<cfargument type="string" name="approve" dbvarname="@approve" required="no">        
        
        <cfargument type="string" name="custom" dbvarname="@custom" required="no">
        <cfargument type="string" name="sortcolumn" dbvarname="@sortcolumn" required="no">
        <cfargument type="string" name="sortorder" dbvarname="@sortorder" required="no">

        <cfset results = "OK">  
		 <cftry>
            <cfstoredproc procedure="custom_select" datasource="#APPLICATION.Datasource#" >         
               <cfif isDefined("Arguments.pageNumber")>
                    <cfprocparam
                    	cfsqltype="cf_sql_integer"
                     	type="In"
                   		value=#Arguments.pageNumber#
                        null="#NOT len(trim(Arguments.pageNumber))#">
                <cfelse>
                	<cfprocparam
                    	cfsqltype="cf_sql_integer"
                      	type="In"
                        null="Yes">
				</cfif> 

           	    <cfif isDefined("Arguments.itemsPerPage")>
                    <cfprocparam
                    	cfsqltype="cf_sql_integer"
                     	type="In"
                   		value=#Arguments.itemsPerPage#
                        null="#NOT len(trim(Arguments.itemsPerPage))#">
                <cfelse>
                	<cfprocparam
                    	cfsqltype="cf_sql_integer"
                      	type="In"
                        null="Yes">
				</cfif> 
      
            	<cfif isDefined("Arguments.assetid")>
                    <cfprocparam
                    	cfsqltype="cf_sql_char"
                     	type="In"
                   		value=#Arguments.assetid#
                        null="#NOT len(trim(Arguments.assetid))#">
                <cfelse>
                	<cfprocparam
                    	cfsqltype="cf_sql_char"
                      	type="In"
                        null="Yes">
				</cfif> 

                <cfif isDefined("Arguments.lastdone_from")>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        value=#Arguments.lastdone_from#
                        null="#NOT len(trim(Arguments.lastdone_from))#">
                <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        null="Yes">
                </cfif> 
                <cfif isDefined("Arguments.lastdone_to")>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        value=#Arguments.lastdone_to#
                        null="#NOT len(trim(Arguments.lastdone_to))#">
               <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        null="Yes">
                </cfif> 
                                        
				<cfif isDefined("Arguments.nextdue_from")>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        value=#Arguments.nextdue_from#
                        null="#NOT len(trim(Arguments.nextdue_from))#">
              <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        null="Yes">
                </cfif> 
                
                <cfif isDefined("Arguments.nextdue_to")>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        value=#Arguments.nextdue_to#
                        null="#NOT len(trim(Arguments.nextdue_to))#">
               <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        null="Yes">
                </cfif> 

              
				<cfif isDefined("Arguments.area")>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
						value=#Arguments.area#
							null="#NOT len(trim(Arguments.area))#">
				<cfelse>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							null="Yes">
				</cfif> 

				<cfif isDefined("Arguments.type")>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
						value=#Arguments.type#
							null="#NOT len(trim(Arguments.type))#">
				<cfelse>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							null="Yes">
				</cfif> 

				<cfif isDefined("Arguments.status")>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
						value=#Arguments.status#
							null="#NOT len(trim(Arguments.status))#">
				<cfelse>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							null="Yes">
				</cfif> 

				<cfif isDefined("Arguments.model")>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
						value=#Arguments.model#
							null="#NOT len(trim(Arguments.model))#">
				<cfelse>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							null="Yes">
				</cfif> 

				<cfif isDefined("Arguments.interval")>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
						value=#Arguments.interval#
							null="#NOT len(trim(Arguments.interval))#">
				<cfelse>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							null="Yes">
				</cfif> 


				<cfif isDefined("Arguments.serial")>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
						value=#Arguments.serial#
							null="#NOT len(trim(Arguments.serial))#">
				<cfelse>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							null="Yes">
				</cfif> 


				<cfif isDefined("Arguments.testprocedure")>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
						value=#Arguments.testprocedure#
							null="#NOT len(trim(Arguments.testprocedure))#">
				<cfelse>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							null="Yes">
				</cfif> 


				<cfif isDefined("Arguments.technician")>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
						value=#Arguments.technician#
							null="#NOT len(trim(Arguments.technician))#">
				<cfelse>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							null="Yes">
				</cfif> 


				<cfif isDefined("Arguments.location")>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
						value=#Arguments.location#
							null="#NOT len(trim(Arguments.location))#">
				<cfelse>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							null="Yes">
				</cfif> 


				<cfif isDefined("Arguments.notes")>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
						value=#Arguments.notes#
							null="#NOT len(trim(Arguments.notes))#">
				<cfelse>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							null="Yes">
				</cfif> 


				<cfif isDefined("Arguments.category")>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
						value=#Arguments.category#
							null="#NOT len(trim(Arguments.category))#">
					<cfelse>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							null="Yes">
				</cfif> 


				<cfif isDefined("Arguments.manufacturer")>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
						value=#Arguments.manufacturer#
							null="#NOT len(trim(Arguments.manufacturer))#">
					<cfelse>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							null="Yes">
				</cfif> 

				<cfif isDefined("Arguments.approve")>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
						value=#Arguments.approve#
							null="#NOT len(trim(Arguments.approve))#">
					<cfelse>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							null="Yes">
				</cfif> 
				

				<cfif isDefined("Arguments.ontimestatus")>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
						value=#Arguments.ontimestatus#
							null="#NOT len(trim(Arguments.ontimestatus))#">
					<cfelse>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							null="Yes">
				</cfif> 

                      
               <cfif isDefined("Arguments.custom")>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        value=#Arguments.custom#>
                <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        null="Yes">
                </cfif> 

 
               <cfif isDefined("Arguments.sortcolumn")>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        value=#Arguments.sortcolumn#
                        null="#NOT len(trim(Arguments.sortcolumn))#">
               <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        null="Yes">
                </cfif> 
                
               <cfif isDefined("Arguments.sortorder")>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        value=#Arguments.sortorder#
                        null="#NOT len(trim(Arguments.sortorder))#">
                <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        null="Yes">
                </cfif> 

                <cfif isDefined("SESSION.User_Area_Filter")>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        value=#SESSION.User_Area_Filter#
						null="#NOT len(trim(SESSION.User_Area_Filter))#">
                <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        null="Yes">
                </cfif>   
                                                      
                <cfprocresult name="custom_select">  
 			</cfstoredproc>
            <cfcatch type="Database">    
                <cfset results = "#cfcatch.detail#">  	
            </cfcatch>
		</cftry>
        <cfreturn custom_select />
    </cffunction>

    <cffunction name="update" output="false" returntype="any" access="public">
        <cfargument type="string" name="lastdone" dbvarname="@lastdone" required="no">
        <cfargument type="string" name="nextdue" dbvarname="@nextdue" required="no">         
		<cfargument type="string" name="assetid" dbvarname="@assetid" required="yes">
		<cfargument type="string" name="area" dbvarname="@area" required="no">
		<cfargument type="string" name="type" dbvarname="@type" required="no">
		<cfargument type="string" name="status" dbvarname="@status" required="no">
		<cfargument type="string" name="model" dbvarname="@model" required="no">
		<cfargument type="string" name="interval" dbvarname="@interval" required="no">
		<cfargument type="string" name="serial" dbvarname="@serial" required="no">
		<cfargument type="string" name="testprocedure" dbvarname="@testprocedure" required="no">
		<cfargument type="string" name="technician" dbvarname="@technician" required="no">
		<cfargument type="string" name="location" dbvarname="@location" required="no">
		<cfargument type="string" name="notes" dbvarname="@notes" required="no">
		<cfargument type="string" name="category" dbvarname="@category" required="no">
		<cfargument type="string" name="manufacturer" dbvarname="@manufacturer" required="no"> 
		<cfargument type="string" name="ontimestatus" dbvarname="@ontimestatus" required="no">      
        <cfargument type="string" name="calibrationperformed" dbvarname="@calibrationperformed" required="no">
 
        <cfset results = "OK">  
		 <cftry>
            <cfstoredproc procedure="Calibration_Update" datasource="#APPLICATION.Datasource#" >         

				<cfprocparam
					cfsqltype="cf_sql_char"
					type="In"
					value=#Arguments.assetid#>

                <cfif isDefined("Arguments.lastdone")>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        value=#Arguments.lastdone#>
                <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        null="Yes">
                </cfif> 
                
 				<cfif isDefined("Arguments.nextdue")>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        value=#Arguments.nextdue#>
              	<cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        null="Yes">
                </cfif> 
              
				<cfif isDefined("Arguments.area")>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
						value=#Arguments.area#>
				<cfelse>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							null="Yes">
				</cfif> 

				<cfif isDefined("Arguments.type")>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							value=#Arguments.type#>
				<cfelse>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							null="Yes">
				</cfif> 

				<cfif isDefined("Arguments.status")>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							value=#Arguments.status#>
				<cfelse>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							null="Yes">
				</cfif> 

				<cfif isDefined("Arguments.model")>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							value=#Arguments.model#>
				<cfelse>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							null="Yes">
				</cfif> 

				<cfif isDefined("Arguments.interval")>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							value=#Arguments.interval#>
				<cfelse>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							null="Yes">
				</cfif> 


				<cfif isDefined("Arguments.serial")>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							value=#Arguments.serial#>
				<cfelse>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							null="Yes">
				</cfif> 


				<cfif isDefined("Arguments.testprocedure")>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							value=#Arguments.testprocedure#>
				<cfelse>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							null="Yes">
				</cfif> 


				<cfif isDefined("Arguments.technician")>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							value=#Arguments.technician#>
				<cfelse>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							null="Yes">
				</cfif> 


				<cfif isDefined("Arguments.location")>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							value=#Arguments.location#>
				<cfelse>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							null="Yes">
				</cfif> 


				<cfif isDefined("Arguments.notes")>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							value=#Arguments.notes#>
				<cfelse>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							null="Yes">
				</cfif> 


				<cfif isDefined("Arguments.category")>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							value=#Arguments.category#>
					<cfelse>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							null="Yes">
				</cfif> 

				<cfif isDefined("Arguments.manufacturer")>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							value=#Arguments.manufacturer#>
					<cfelse>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							null="Yes">
				</cfif> 

				<cfif isDefined("Arguments.ontimestatus")>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							value=#Arguments.ontimestatus#>
					<cfelse>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							null="Yes">
				</cfif> 

				<cfif isDefined("Arguments.calibrationperformed")>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							value=#Arguments.calibrationperformed#>
					<cfelse>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							null="Yes">
				</cfif> 
 
 
             
                <cfprocparam
                    cfsqltype="CF_SQL_VARCHAR"
                    type="Out" 
                    value=""
                    variable="idenity"
                    >

                <cfprocparam
                    cfsqltype="CF_SQL_VARCHAR"
                    type="Out" 
                    value=""
                    variable="sqlRequest"
                    >
 
                <cfprocparam
                    cfsqltype="CF_SQL_VARCHAR"
                    type="Out" 
                    value=""
                    variable="nextduedate"
                    >
 
                <cfprocparam
                    cfsqltype="CF_SQL_VARCHAR"
                    type="Out" 
                    value=""
                    variable="updatednotes"
                    >
 
                <cfprocparam
                    cfsqltype="CF_SQL_VARCHAR"
                    type="Out" 
                    value=""
                    variable="calibrated"
                    >
                                     
                <cfprocresult name="update">  
 			</cfstoredproc>
            <cfcatch type="Database">    
                <cfset results = "#cfcatch.detail#">  	
            </cfcatch>
		</cftry>
        <cfscript>
		 	returnResult = StructNew();
			returnResult['results'] = results;
			returnResult['idenity'] = idenity;
			returnResult['sqlRequest'] = sqlRequest;
			returnResult['nextduedate'] = nextduedate;
			returnResult['calibrationperformed'] = calibrated;
			returnResult['updatednotes'] = updatednotes;
		</cfscript>  

        <cfreturn returnResult />
    </cffunction>

	<cffunction name="DropDowns" output="false" returntype="query" access="public">    
        <cfargument type="string" name="storedProcedure" required="yes">
            <cfstoredproc procedure="#Arguments.storedProcedure#" datasource="#APPLICATION.Datasource#" >
  				<cfprocresult name="qry_getSelect">  
           </cfstoredproc>
         <cfreturn qry_getSelect />
	</cffunction>     

	<cffunction name="insert" output="false" returntype="any" access="public">
        <cfargument type="string" name="lastdone" dbvarname="@lastdone_from" required="no">
        <cfargument type="string" name="nextdue" dbvarname="@nextdue_from" required="no">      
		<cfargument type="string" name="assetid" dbvarname="@assetid" required="yes">
		<cfargument type="string" name="area" dbvarname="@area" required="no">
		<cfargument type="string" name="type" dbvarname="@type" required="no">
		<cfargument type="string" name="status" dbvarname="@status" required="no">
		<cfargument type="string" name="model" dbvarname="@model" required="no">
		<cfargument type="string" name="interval" dbvarname="@interval" required="no">
		<cfargument type="string" name="serial" dbvarname="@serial" required="no">
		<cfargument type="string" name="testprocedure" dbvarname="@testprocedure" required="no">
		<cfargument type="string" name="technician" dbvarname="@technician" required="no">
		<cfargument type="string" name="location" dbvarname="@location" required="no">
		<cfargument type="string" name="notes" dbvarname="@notes" required="no">
		<cfargument type="string" name="category" dbvarname="@category" required="no">
		<cfargument type="string" name="manufacturer" dbvarname="@manufacturer" required="no">        
		<cfargument type="string" name="ontimestatus" dbvarname="@ontimestatus" required="no">      
        <cfargument type="string" name="calibrationperformed" dbvarname="@calibrationperformed" required="no">

        <cfset results = "OK">  
		 <cftry>
            <cfstoredproc procedure="Calibration_Insert" datasource="#APPLICATION.Datasource#" >               
            	<cfif isDefined("Arguments.assetid")>
                    <cfprocparam
                    	cfsqltype="cf_sql_char"
                     	type="In"
                   		value=#Arguments.assetid#
                        null="#NOT len(trim(Arguments.assetid))#">
                <cfelse>
                	<cfprocparam
                    	cfsqltype="cf_sql_char"
                      	type="In"
                        null="Yes">
				</cfif> 

                <cfif isDefined("Arguments.lastdone")>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        value=#Arguments.lastdone#
                        null="#NOT len(trim(Arguments.lastdone))#">
                <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        null="Yes">
                </cfif> 
                                        
				<cfif isDefined("Arguments.nextdue")>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        value=#Arguments.nextdue#
                        null="#NOT len(trim(Arguments.nextdue))#">
              <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        null="Yes">
                </cfif> 
              
				<cfif isDefined("Arguments.area")>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							value=#Arguments.area#
							null="#NOT len(trim(Arguments.area))#">
				<cfelse>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							null="Yes">
				</cfif> 

				<cfif isDefined("Arguments.type")>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							value=#Arguments.type#
							null="#NOT len(trim(Arguments.type))#">
				<cfelse>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							null="Yes">
				</cfif> 

				<cfif isDefined("Arguments.status")>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							value=#Arguments.status#
							null="#NOT len(trim(Arguments.status))#">
				<cfelse>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							null="Yes">
				</cfif> 

				<cfif isDefined("Arguments.model")>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							value=#Arguments.model#
							null="#NOT len(trim(Arguments.model))#">
				<cfelse>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							null="Yes">
				</cfif> 

				<cfif isDefined("Arguments.interval")>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							value=#Arguments.interval#
							null="#NOT len(trim(Arguments.interval))#">
				<cfelse>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							null="Yes">
				</cfif> 

				<cfif isDefined("Arguments.serial")>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							value=#Arguments.serial#
							null="#NOT len(trim(Arguments.serial))#">
				<cfelse>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							null="Yes">
				</cfif> 

				<cfif isDefined("Arguments.testprocedure")>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							value=#Arguments.testprocedure#
							null="#NOT len(trim(Arguments.testprocedure))#">
				<cfelse>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							null="Yes">
				</cfif> 

				<cfif isDefined("Arguments.technician")>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							value=#Arguments.technician#
							null="#NOT len(trim(Arguments.technician))#">
				<cfelse>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							null="Yes">
				</cfif> 

				<cfif isDefined("Arguments.location")>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							value=#Arguments.location#
							null="#NOT len(trim(Arguments.location))#">
				<cfelse>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							null="Yes">
				</cfif> 

				<cfif isDefined("Arguments.notes")>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							value=#Arguments.notes#
							null="#NOT len(trim(Arguments.notes))#">
				<cfelse>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							null="Yes">
				</cfif> 

				<cfif isDefined("Arguments.category")>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
						value=#Arguments.category#
							null="#NOT len(trim(Arguments.category))#">
					<cfelse>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							null="Yes">
				</cfif> 

				<cfif isDefined("Arguments.manufacturer")>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							value=#Arguments.manufacturer#
							null="#NOT len(trim(Arguments.manufacturer))#">
					<cfelse>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							null="Yes">
				</cfif> 

				<cfif isDefined("Arguments.ontimestatus")>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							value=#Arguments.ontimestatus#
							null="#NOT len(trim(Arguments.ontimestatus))#">
					<cfelse>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							null="Yes">
				</cfif> 
 
				<cfif isDefined("Arguments.calibrationperformed")>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							value=#Arguments.calibrationperformed#>
					<cfelse>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							null="Yes">
				</cfif> 
                                                                                                              
                <cfprocparam
                    cfsqltype="CF_SQL_VARCHAR"
                    type="Out" 
                    value=""
                    variable="idenity"
                    >

                <cfprocparam
                    cfsqltype="CF_SQL_VARCHAR"
                    type="Out" 
                    value=""
                    variable="sqlRequest"
                    >
                    
               <cfprocparam
                    cfsqltype="CF_SQL_VARCHAR"
                    type="Out" 
                    value=""
                    variable="nextduedate"
                    >
                    
                <cfprocparam
                    cfsqltype="CF_SQL_VARCHAR"
                    type="Out" 
                    value=""
                    variable="updatednotes"
                    >
 
                <cfprocparam
                    cfsqltype="CF_SQL_VARCHAR"
                    type="Out" 
                    value=""
                    variable="calibrated"
                    >                                        
                <cfprocresult name="insert">  
 			</cfstoredproc>
            <cfcatch type="Database">    
                <cfset results = "#cfcatch.detail#">  	
            </cfcatch>
		</cftry>
        <cfscript>
		 	returnResult = StructNew();
			returnResult['results'] = results;
			try {
				returnResult['idenity'] = idenity;
				returnResult['sqlRequest'] = sqlRequest;
				returnResult['nextduedate'] = nextduedate;
				returnResult['calibrationperformed'] = calibrated;
				returnResult['updatednotes'] = updatednotes;
			}catch(any e){
				//returnResult['idenity'] = '';
			}
		</cfscript>  

        <cfreturn returnResult />
    </cffunction>
	
    <cffunction name="custom_report" output="false" returntype="query" access="public">
        <cfargument type="any" name="pageNumber" dbvarname="@pageNumber" required="no">
        <cfargument type="any" name="itemsPerPage" dbvarname="@itemsPerPage" required="no">
        <cfargument type="string" name="lastdone_from" dbvarname="@lastdone_from" required="no">
        <cfargument type="string" name="lastdone_to" dbvarname="@lastdone_to" required="no">
        <cfargument type="string" name="nextdue_from" dbvarname="@nextdue_from" required="no">
        <cfargument type="string" name="nextdue_to" dbvarname="@nextdue_to" required="no">        
		<cfargument type="string" name="assetid" dbvarname="@assetid" required="no">
		<cfargument type="string" name="area" dbvarname="@area" required="no">
		<cfargument type="string" name="type" dbvarname="@type" required="no">
		<cfargument type="string" name="status" dbvarname="@status" required="no">
		<cfargument type="string" name="model" dbvarname="@model" required="no">
		<cfargument type="string" name="interval" dbvarname="@interval" required="no">
		<cfargument type="string" name="serial" dbvarname="@serial" required="no">
		<cfargument type="string" name="testprocedure" dbvarname="@testprocedure" required="no">
		<cfargument type="string" name="technician" dbvarname="@technician" required="no">
		<cfargument type="string" name="location" dbvarname="@location" required="no">
		<cfargument type="string" name="notes" dbvarname="@notes" required="no">
		<cfargument type="string" name="category" dbvarname="@category" required="no">
		<cfargument type="string" name="manufacturer" dbvarname="@manufacturer" required="no">        
        
        <cfargument type="string" name="custom" dbvarname="@custom" required="no">
        <cfargument type="string" name="sortcolumn" dbvarname="@sortcolumn" required="no">
        <cfargument type="string" name="sortorder" dbvarname="@sortorder" required="no">

        <cfset results = "OK">  
		 <cftry>
            <cfstoredproc procedure="custom_report_select" datasource="#APPLICATION.Datasource#" >         
               <cfif isDefined("Arguments.pageNumber")>
                    <cfprocparam
                    	cfsqltype="cf_sql_integer"
                     	type="In"
                   		value=#Arguments.pageNumber#
                        null="#NOT len(trim(Arguments.pageNumber))#">
                <cfelse>
                	<cfprocparam
                    	cfsqltype="cf_sql_integer"
                      	type="In"
                        null="Yes">
				</cfif> 

           	    <cfif isDefined("Arguments.itemsPerPage")>
                    <cfprocparam
                    	cfsqltype="cf_sql_integer"
                     	type="In"
                   		value=#Arguments.itemsPerPage#
                        null="#NOT len(trim(Arguments.itemsPerPage))#">
                <cfelse>
                	<cfprocparam
                    	cfsqltype="cf_sql_integer"
                      	type="In"
                        null="Yes">
				</cfif> 
      
            	<cfif isDefined("Arguments.assetid")>
                    <cfprocparam
                    	cfsqltype="cf_sql_char"
                     	type="In"
                   		value=#Arguments.assetid#
                        null="#NOT len(trim(Arguments.assetid))#">
                <cfelse>
                	<cfprocparam
                    	cfsqltype="cf_sql_char"
                      	type="In"
                        null="Yes">
				</cfif> 

                <cfif isDefined("Arguments.lastdone_from")>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        value=#Arguments.lastdone_from#
                        null="#NOT len(trim(Arguments.lastdone_from))#">
                <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        null="Yes">
                </cfif> 
                <cfif isDefined("Arguments.lastdone_to")>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        value=#Arguments.lastdone_to#
                        null="#NOT len(trim(Arguments.lastdone_to))#">
               <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        null="Yes">
                </cfif> 
                                        
				<cfif isDefined("Arguments.nextdue_from")>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        value=#Arguments.nextdue_from#
                        null="#NOT len(trim(Arguments.nextdue_from))#">
              <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        null="Yes">
                </cfif> 
                
                <cfif isDefined("Arguments.nextdue_to")>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        value=#Arguments.nextdue_to#
                        null="#NOT len(trim(Arguments.nextdue_to))#">
               <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        null="Yes">
                </cfif> 

              
				<cfif isDefined("Arguments.area")>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
						value=#Arguments.area#
							null="#NOT len(trim(Arguments.area))#">
				<cfelse>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							null="Yes">
				</cfif> 

				<cfif isDefined("Arguments.type")>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
						value=#Arguments.type#
							null="#NOT len(trim(Arguments.type))#">
				<cfelse>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							null="Yes">
				</cfif> 

				<cfif isDefined("Arguments.status")>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
						value=#Arguments.status#
							null="#NOT len(trim(Arguments.status))#">
				<cfelse>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							null="Yes">
				</cfif> 

				<cfif isDefined("Arguments.model")>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
						value=#Arguments.model#
							null="#NOT len(trim(Arguments.model))#">
				<cfelse>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							null="Yes">
				</cfif> 

				<cfif isDefined("Arguments.interval")>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
						value=#Arguments.interval#
							null="#NOT len(trim(Arguments.interval))#">
				<cfelse>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							null="Yes">
				</cfif> 


				<cfif isDefined("Arguments.serial")>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
						value=#Arguments.serial#
							null="#NOT len(trim(Arguments.serial))#">
				<cfelse>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							null="Yes">
				</cfif> 


				<cfif isDefined("Arguments.testprocedure")>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
						value=#Arguments.testprocedure#
							null="#NOT len(trim(Arguments.testprocedure))#">
				<cfelse>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							null="Yes">
				</cfif> 


				<cfif isDefined("Arguments.technician")>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
						value=#Arguments.technician#
							null="#NOT len(trim(Arguments.technician))#">
				<cfelse>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							null="Yes">
				</cfif> 


				<cfif isDefined("Arguments.location")>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
						value=#Arguments.location#
							null="#NOT len(trim(Arguments.location))#">
				<cfelse>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							null="Yes">
				</cfif> 


				<cfif isDefined("Arguments.notes")>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
						value=#Arguments.notes#
							null="#NOT len(trim(Arguments.notes))#">
				<cfelse>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							null="Yes">
				</cfif> 


				<cfif isDefined("Arguments.category")>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
						value=#Arguments.category#
							null="#NOT len(trim(Arguments.category))#">
					<cfelse>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							null="Yes">
				</cfif> 


				<cfif isDefined("Arguments.manufacturer")>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
						value=#Arguments.manufacturer#
							null="#NOT len(trim(Arguments.manufacturer))#">
					<cfelse>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							null="Yes">
				</cfif> 


				<cfif isDefined("Arguments.ontimestatus")>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
						value=#Arguments.ontimestatus#
							null="#NOT len(trim(Arguments.ontimestatus))#">
					<cfelse>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							null="Yes">
				</cfif> 

                      
               <cfif isDefined("Arguments.custom")>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        value=#Arguments.custom#>
                <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        null="Yes">
                </cfif> 

 
               <cfif isDefined("Arguments.sortcolumn")>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        value=#Arguments.sortcolumn#
                        null="#NOT len(trim(Arguments.sortcolumn))#">
               <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        null="Yes">
                </cfif> 
                
               <cfif isDefined("Arguments.sortorder")>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        value=#Arguments.sortorder#
                        null="#NOT len(trim(Arguments.sortorder))#">
                <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        null="Yes">
                </cfif> 
                                        
                <cfprocresult name="custom_select">  
 			</cfstoredproc>
            <cfcatch type="Database">    
                <cfset results = "#cfcatch.detail#">  	
            </cfcatch>
		</cftry>
        <cfreturn custom_select />
    </cffunction>
    
	<cffunction name="Admin" output="false" returntype="any" access="public">
        <cfargument type="string" name="cmd" dbvarname="@cmd" required="yes">
        <cfargument type="string" name="newvalue" dbvarname="@newvalue" required="no">      
		<cfargument type="string" name="previousValue" dbvarname="@previousValue" required="no">
		<cfargument type="string" name="table" dbvarname="@table" required="yes">

        <cfset results = "OK">  
		 <cftry>
            <cfstoredproc procedure="admin" datasource="#APPLICATION.Datasource#" >               
            	<cfif isDefined("Arguments.cmd")>
                    <cfprocparam
                    	cfsqltype="cf_sql_char"
                     	type="In"
                   		value=#Arguments.cmd#
                        null="#NOT len(trim(Arguments.cmd))#">
                <cfelse>
                	<cfprocparam
                    	cfsqltype="cf_sql_char"
                      	type="In"
                        null="Yes">
				</cfif> 

                <cfif isDefined("Arguments.newvalue")>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        value=#Arguments.newvalue#
                        null="#NOT len(trim(Arguments.newvalue))#">
                <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        null="Yes">
                </cfif> 
                                        
				<cfif isDefined("Arguments.previousValue")>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        value=#Arguments.previousValue#
                        null="#NOT len(trim(Arguments.previousValue))#">
              <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        null="Yes">
                </cfif> 
              
				<cfif isDefined("Arguments.table")>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							value=#Arguments.table#
							null="#NOT len(trim(Arguments.table))#">
				<cfelse>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							null="Yes">
				</cfif> 
                                                                                                              
                <cfprocparam
                    cfsqltype="CF_SQL_VARCHAR"
                    type="Out" 
                    value=""
                    variable="status"
                    >
                                        
                <cfprocresult name="admin">  
 			</cfstoredproc>
            <cfcatch type="Database">    
                <cfset results = "#cfcatch.detail#">  	
            </cfcatch>
		</cftry>
        <cfscript>
		 	returnResult = StructNew();
			returnResult['results'] = results;
			returnResult['cmd'] = Arguments.cmd;
			returnResult['newvalue'] = Arguments.newvalue;
			returnResult['previousValue'] = Arguments.previousValue;
			returnResult['table'] = Arguments.table;
			returnResult['dropdown'] = admin;
			returnResult['status'] = status;
		</cfscript>  

        <cfreturn returnResult />
    </cffunction>
    
	<cffunction name="UserLoginInfo" output="false" returntype="any" access="public">
        <cfargument type="string" name="userid" dbvarname="@userid" required="yes">
        <cfargument type="string" name="company" dbvarname="@company" required="no">      
		<cfargument type="string" name="department" dbvarname="@department" required="no">
		<cfargument type="string" name="area_filter" dbvarname="@area_filter" required="no">
		<cfargument type="string" name="location" dbvarname="@location" required="no">
		<cftry>
            <cfstoredproc procedure="userinfo" datasource="#APPLICATION.Datasource#" >               
            	<cfif isDefined("Arguments.userid")>
                    <cfprocparam
                    	cfsqltype="cf_sql_char"
                     	type="In"
                   		value=#Arguments.userid#
                        null="#NOT len(trim(Arguments.userid))#">
                <cfelse>
                	<cfprocparam
                    	cfsqltype="cf_sql_char"
                      	type="In"
                        null="Yes">
				</cfif> 

                <cfif isDefined("Arguments.company")>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        value=#Arguments.company#
                        null="#NOT len(trim(Arguments.company))#">
                <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        null="Yes">
                </cfif> 
                                        
				<cfif isDefined("Arguments.department")>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        value=#Arguments.department#
                        null="#NOT len(trim(Arguments.department))#">
               <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        null="Yes">
               </cfif> 

				<cfif isDefined("Arguments.area_filter")>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        value=#Arguments.area_filter#
                        null="#NOT len(trim(Arguments.area_filter))#">
               <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        null="Yes">
               </cfif>                                                                                                           

			   <cfif isDefined("Arguments.location")>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        value=#Arguments.location#
                        null="#NOT len(trim(Arguments.location))#">
               <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        null="Yes">
               </cfif>                                                                                                           
              <!--- <cfprocresult name="admin">  --->
 			</cfstoredproc>
            <cfcatch type="Database">    
                <cfset results = "#cfcatch.detail#">  	
            </cfcatch>
		</cftry>
    </cffunction>
    
    <cffunction name="Approval" output="false" returntype="any" access="public">
        <cfargument type="string" name="assetid" dbvarname="@assetid" required="yes">
        <cfset results = "OK">  
		 <cftry>
            <cfstoredproc procedure="approval" datasource="#APPLICATION.Datasource#" >               
            	<cfif isDefined("Arguments.assetid")>
                    <cfprocparam
                    	cfsqltype="cf_sql_char"
                     	type="In"
                   		value=#Arguments.assetid#
                        null="#NOT len(trim(Arguments.assetid))#">
                <cfelse>
                	<cfprocparam
                    	cfsqltype="cf_sql_char"
                      	type="In"
                        null="Yes">
				</cfif> 

                                        
                <cfprocresult name="approval">  
 			</cfstoredproc>
            <cfcatch type="Database">    
                <cfset results = "#cfcatch.detail#">  	
            </cfcatch>
		</cftry>
        <cfscript>
		 	returnResult = StructNew();
			returnResult['results'] = results;
			returnResult['assetid'] = Arguments.assetid;
		</cfscript>  

        <cfreturn returnResult />
    </cffunction>
    
 
	<cffunction name="MonthlyNotice" output="false" returntype="query" access="public">    
		<cfstoredproc procedure="monthly30daynotice" datasource="#APPLICATION.Datasource#" >
			<cfprocresult name="qry_getSelect">  
		</cfstoredproc>
		<cfreturn qry_getSelect />
	</cffunction>     	
		
			
				
					
						
							
								
										
</cfcomponent>