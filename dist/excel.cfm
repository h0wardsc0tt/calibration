<cfparam name="FORM.lead_filter" default="">
<cfparam name="FORM.excel_columns" default="">
<cfparam name="FORM.excel_leads_excluded" default="">
<cfparam name="FORM.show_excel_header" default="yes">

	<cfscript>
        CustInfoObj = CreateObject("component", "_cfcs.CustInfo");
		jsonFilter = #DeserializeJSON(FORM.lead_filter)#;
		excel_columns = #DeserializeJSON(FORM.excel_columns)#;
		leads_excluded = #DeserializeJSON(FORM.excel_leads_excluded)#;
		seperator = '';
		select_columns = 'SELECT ';
		for (obj in excel_columns)
		{			
			col = "lead_" &  obj['column'];
			switch(obj['column']){
				case "NAME":
					col = "LEAD_FIRSTNAME + ' ' + LEAD_LASTNAME";
				break;
				case "ENTERED DATE":
					col = "LEAD_Created_DTS";
				break;
				case "PHONE":
					col = "dbo.fnFormatPhoneNumber(LEAD_PHONE)";
				break;
				case "ALTPHONE":
					col = "dbo.fnFormatPhoneNumber(LEAD_ALTPHONE)";
				break;
				case "FAX":
					col = "dbo.fnFormatPhoneNumber(LEAD_FAX)";
				break;
				case "STATE":
					col = "dbo.fnstate_abbr(LEAD_STATE)";
				break;
			}
			select_columns = select_columns & seperator & col & " AS [" &  ReReplaceNoCase(obj["name"], ' ', ' ', 'all') & "]";	
			seperator = ", ";
		}
		seperator = '';
		excluded = '';
		for(lead in leads_excluded){
			excluded = excluded & seperator & lead['id'];
			seperator = ',';
		}

		jsonFilter['notIn'] = excluded;
		jsonFilter['excelSelect'] = select_columns;
 		jsonFilter['storedProcedure'] = 'dbo.[Leads_Select]';
        returnSelect = CustInfoObj.LeadsSelect(argumentCollection = jsonFilter);	
        tempPath  =   GetTempDirectory() & CreateUuid() & ".xls";
        exceldoc = spreadsheetNewFromQuery(data=returnSelect,path=tempPath);
        fileName = "LeadsExport_" & DateFormat(Now(), "mmddyy") & TimeFormat(Now(), "_HHmmss") & ".xls";   
    </cfscript>
    
    <cfheader name="Content-Disposition" value="attachment; filename=#fileName#">
    <cfcontent  
        type = "application/msexcel"  
        file = "#tempPath#"  
        deleteFile = "Yes">
 
    <cffunction name="spreadsheetNewFromQuery" output="false">
        <cfargument name="data" type="query" required="true">
        <cfargument name="path" type="string" required="true">
        <cfargument name="sheetName" type="string" default="Sheet1">
        <cfargument name="removeHeaderRow" type="boolean" default="false">
        <!---<cfset var tempPath  =   GetTempDirectory() & CreateUuid() & ".xls">--->
        <cfspreadsheet action="write" filename="#path#" query="data" sheetname="#sheetName#" overwrite="true">
        <!---<cfscript>
            var spreadsheetObject = SpreadsheetRead(path);
            //FileDelete( tempPath );
            if( removeHeaderRow ){
                SpreadsheetShiftRows( spreadsheetObject,2,data.recordcount+1,-1 );
            }
            return spreadsheetObject;
        </cfscript>--->
		
    </cffunction>