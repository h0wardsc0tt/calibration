<cfinclude template="/serverSettings.cfm">
<cfsetting showdebugoutput="yes">


<cfset tmp_file_name = "#GetTempDirectory()#/#CreateUUID()#.pdf">
<cfset logoImage ='/images/hme-companies.png'>

<cfscript>		
	obj = CreateObject("component", "_cfcs.calibration");
	returnSelect = obj.MonthlyNotice();
</cfscript>


<cfscript>
//Clearcom_ME;
data = [
{area: "ALAMEDA", email:"Amado.Bautista@Clearcom.com", name:"Amado Bautista"},
{area: "CC MANF", email:"David.VanBebber@Clearcom.com", name:"David Van Bebber"},
{area: "CC", email:"David.VanBebber@Clearcom.com", name:"David Van Bebber"},
{area: "FS", email:"TroyD@hme.com;mbath@hme.com", name:"Troy Daen, Mark Bath"},
{area: "HME MANF", email:"CrisM@hme.com;winguito@hme.com;MichaelG@hme.com", name:"Michael Graf, Woodie Inguito, Cris Manglinong"},
{area: "EE", email:"NiroshW@hme.com", name:"Nirosh Wijayaratne"},
{area: "QA", email:"mhoward@hme.com;DDay@hme.com;DRiley@hme.com", name:"Michael Howard, David Day, David Riley"},
{area: "SMT", email:"acruzada@hme.com;GPapas@hme.com", name:"Aldrin Cruzada, Gary Papas"}
];
</cfscript>

<cfloop array="#data#" index="i">
	<cfquery dbtype="query" name="areas">
		select *
		from returnSelect
		where Area = '#i.area#'
	</cfquery>
	<cfif areas.recordCount>
		<cftry>

			<!---<cfmail to="hscott@hme.com" cc="hscott@hme.com" from="no-reply@hme.com" subject="#i.area# Calibration within 30 days" type="html">--->
				<cfmail to="#i.email#" cc="" from="no-reply@hme.com" subject="#i.area# Calibration within 30 days" type="html">        
				   <p> 
						 <img src="cid:logo" alt="" /><br /> 
						 <span style="font-size:15px; color:##CCC8C8;">TEST AND MEASUREMENT EQUIPMENT CALIBRATION</span><br>
				   </p> 

					<div style="margin-left:5px; margin-right:5px"><p>Good Morning #i.name#</p><p>The following list of items require calibtation within the next 30 days.</p>   
					<cfloop query = "areas">
						Asset ID:<a href="#APPLICATION.rootURL#/?asset=#assetid#">#assetid#</a> Type:#type# Due Date: #DateFormat(nextdue, 'mm/dd/yyyy')# <br>
					</cfloop>
					<p>If you have any questions or updates, please do not hesitate to contact David Day or Michael Howard.</p>     
					<!---<p>Regards,</p><p>David Day<br />QA Inspection Leader<br />x4058</p></div>--->
					<cfmailparam  
						 file="#ExpandPath('/images/hme-companies.png')#" 
						 contentid="logo"  
						 disposition="inline" 
					/> 		
				</cfmail>
			<cfcatch type="Application">
			</cfcatch>
		</cftry>
	</cfif>
</cfloop>