
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container" style="padding-right: 0px;padding-left: 0px;">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span> 
                </button>
                <a class="navbar-brand navbar-logo corp-images"><img src="./images/hme-companies.png" alt="HME" height="35" style="display:block;margin-top: 3px;" /></a>                
            </div>
           <div class="collapse navbar-collapse" id="myNavbar">
            
               <form method="post" class="navbar-form navbar-right" id="SearchForm" style="padding: 0px 0px;" 
               		onsubmit="asset_Number_Lookup();return false;">
                    <div class="search" style="padding-top:7px;">                        
                        <input type="search" id="Search" name="Search" class="form-control fai-lookup" placeholder="Asset ID...">
                        <span class="fa fa-search" onClick="asset_Number_Lookup();"></span>
                    </div>
                </form>
                 <ul class="nav navbar-nav" style="padding-right:3px;margin-top: 15px;">
                  <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#"><button id="header_button" class="btn btn-primary">Menu
                    <span class="caret"></span></button></a>
                     <ul class="dropdown-menu"  style="margin-right:3px;">
                      <li style="padding: 3px 5px 0px 5px;color:#180BB8;">Queries......</li>
                      <li><a href="#" style="padding: 3px 5px 0px 15px;" onclick="fai_dropdown_filter('currentmonth');">Current Month</a></li>

 <!---                     <li style="padding: 3px 5px 0px 5px;color:#180BB8;">Reports......</li>
                      <li><a href="#" style="padding: 3px 5px 0px 15px;" onclick="complete_notice()">FAI Report</a></li>
                      <li><a href="#" style="padding: 3px 5px 0px 15px;" onclick="fai_traveler()">FAI Traveler</a></li>
                      <li><a href="#" style="padding: 3px 5px 0px 15px;" onclick="cdb_modal_display('pdf');">Closed the Day Before</a></li>
                      <li><a href="#" style="padding: 3px 5px 0px 15px;" onclick="fai_report('work_in_progress_report');">Work In Progress</a></li>
                      <li><a href="#" style="padding: 3px 5px 0px 15px;" onclick="fai_report('completed_by_ri_date_report');">Completed By RI Date</a></li>
                      <li style="padding: 3px 5px 0px 5px;color:#180BB8;">Queries......</li>
                      <li><a href="#" style="padding: 3px 5px 0px 15px;" onclick="fai_dropdown_filter('aqe');">Awaiting QE's</a></li>
                      <li><a href="#" style="padding: 3px 5px 0px 15px;" onclick="cdb_modal_display('filter');">Closed the Day Before</a></li>
                      <li><a href="#" style="padding: 3px 5px 0px 15px;" onclick="fai_dropdown_filter('ewip');">Engineering WIP</a></li>
                      <li><a href="#" style="padding: 3px 5px 0px 15px;" onclick="show_request_fai_part_number_modal();">FPY by Supplier</a></li>
                      <li><a href="#" style="padding: 3px 5px 0px 15px;" onclick="fai_dropdown_filter('rc');">Ready to Close</a></li>
                      <li><a href="#" style="padding: 3px 5px 0px 15px;" onclick="fai_dropdown_filter('riic');">RI Inspection Complete</a></li>
                      <li><a href="#" style="padding: 3px 5px 0px 15px;" onclick="fai_dropdown_filter('rinc');">RI not Complete</a></li>
                      <li><a href="#" style="padding: 3px 5px 0px 15px;" onclick="fai_dropdown_filter('wip');">Work In Progress</a></li>
                      <li><a href="#" style="padding: 3px 5px 0px 15px;" onclick="fai_dropdown_filter('wipnonpcb');">Work In Progress NONPCB's</a></li> 
                      <li><a href="#" style="padding: 3px 5px 0px 15px;" onclick="fai_dropdown_filter('wippcb');">Work In Progress PCB's</a></li> --->
                      <li style="padding: 3px 5px 0px 5px;color:#180BB8;">Application......</li>
                      <li><a href="#" style="padding: 3px 5px 0px 15px;" onclick="admin();">Admin</a></li>
                      <li><a href="#" style="padding: 3px 5px 0px 15px;" onclick="logout();">Logout</a></li>
                    </ul>
                  </li>
                </ul>
               
                <div class="nav navbar-nav navbar-right nav-buttons" style="padding-top:15px;">
                <div id="status_box" class="status-box" style="position:absolute; top:50px; height:30px; width:200px; padding-top:5px; border:solid 1px #CFC7C8; background-color:#fff; color:#230CF1; vertical-align:middle; text-align:center; display:none;"></div>
                <button class="btn btn-primary hidden report-btn"  onclick="email_completion_notification_report();">Completion Report</button>
                <button class="btn btn-primary report-page hidden" title="Return to Search Page" onclick="search_page();"><span class="glyphicon glyphicon-search"></span></button>
                <button class="btn btn-primary search-page hidden" title="Return to Calibration Report Page" onclick="report_page();"><span class="glyphicon glyphicon-list-alt"></span></button>
                <button id="fai_new_btn" class="btn btn-primary" title="Create New Calibration Report"  onClick="new_fai();"><span class="glyphicon glyphicon-plus"></span></button>
                <button id="fai_save_btn" class="btn btn-primary report-page hidden" title="Save Calibration Report" onClick="update_fai();">Save</button>
                <button id="fai_approve_btn" faiid="" class="btn btn-primary approve hidden" title="Approve Calibration Report" onClick="approve_fai();">Approve</button>
               

                 <ul class="navbar-nav" style="display:block;">
                  <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#"><button style="margin-right: 5px;" class="btn btn-primary ">
                	<span class="glyphicon glyphicon-envelope"></span></button></a>
                     <ul class="dropdown-menu"  style="margin-right:3px;">
                      <li><a href="#" style="padding: 3px 5px 0px 5px;" onclick="blank_email();">Empty Email Form</a></li>
<!---                      <li><a href="#" style="padding: 3px 5px 0px 5px;" onclick="email_completion_notification_report();">Completion Notification Report</a></li>
                      <li><a href="#" style="padding: 3px 5px 0px 5px;" onclick="email_fai_report('work_in_progress_report');">Work In Progress</a></li>
                       <li><a href="#" style="padding: 3px 5px 0px 5px;" onclick="email_fai_report('awaiting_qe_report')";>Awaiting QE's</a></li>
                      <li><a href="#" style="padding: 3px 5px 0px 5px;" onclick="email_fai_report('engineering_wip_report')";>Engineering WIP</a></li>
--->                     </ul>
                  </li>
                </ul>

                <!--
                <button class="btn btn-primary report-page hidden new-fai single-fai" onClick="$('.email-element').show();">
                	<span class="glyphicon glyphicon-envelope"></span></button>
                 -->   
                    
                <button class="btn btn-primary report-page hidden new-fai single-fai always-hidden"><span class="glyphicon glyphicon-fast-backward"></span></button>
                <button class="btn btn-primary report-page hidden new-fai single-fai always-hidden"><span class="glyphicon glyphicon-step-backward"></span></button>
                <button class="btn btn-primary report-page hidden new-fai single-fai always-hidden"><span class="glyphicon glyphicon-step-forward"></span></button>
                <div style="padding-right:5px; display:inline-block;"><button class="btn btn-primary report-page hidden new-fai single-fai always-hidden"><span class="glyphicon glyphicon-fast-forward"></span></button></div>

                </div>
                 <!---
                <ul class="nav navbar-nav navbar-right" >
                 <li> <a href="javascript:void(0);"><button id="CSP_Next_Page" class="btn btn-primary">FAI Complete Notice</button></a>    
                </ul>--->
              
       </div>               
    </div>
</nav>
<div id="spinner_dev"></div> 
<div style="height:60px;"></div>    



