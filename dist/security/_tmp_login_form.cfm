
<cfif NOT StructKeyExists(SESSION, "loginAttempts")>
	<cfset SESSION.loginAttempts = 0>
</cfif>
<cfparam name="URL.asset" default="">
<cfset assetid = "">
<cfif URL.asset neq "">
	<cfset assetid = "&asset=#URL.asset#">
</cfif>

<div class="container container-table">
	<div id="Content" class="content-login">
        <form action="./?pg=Login&st=Validate<cfoutput>#assetid#</cfoutput>" class="form-horizontal" method="POST">
            <cfif ERR.ErrorFound>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 col-centered col-login-error login-error animated fadeIn">
                    <div class="form-message form-error">
                        <ul>
                            <cfloop list="#ERR.ErrorMessage#" index="thisError"><li><cfoutput>#thisError#</cfoutput></li></cfloop>
                        </ul>
                    </div>
                </div>
            </cfif>
        
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 col-centered container-login-small <cfif NOT ERR.ErrorFound>animated fadeInDown</cfif>">
                <div class="col-lg-12 col-login-right">
                    <div class="form-group">
                        <div class="col-lg-4"></div>
                        <div class="col-lg-4">
                            <div class="login-logo">
                            	<img src="./images/HME-Logo-large.png" alt="HME" width="95" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
						<div class="col-lg-2"></div>
                        <div class="col-lg-8 col-min-pad">
                            <input type="text" maxlength="100" name="Username" class="form-control" value="" placeholder="Username" />
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-2"></div>
                        <div class="col-lg-8 col-min-pad">
                            <input type="password" maxlength="16" name="Password" class="form-control" value="" placeholder="Password"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-2"></div>
                        <div class="col-lg-8 col-min-pad">
                            <input type="submit" value="Login" class="btn btn-primary form-control"/>
                        </div>
                    </div>
                     <div class="form-group">
						<div class="col-lg-2"></div>
                        <div class="col-lg-8 login-copy">
                            &copy;2019 HM Electronics, Inc., all rights reserved.
                        </div>
                    </div>
                </div>
            </div>
        </form>