<cfcomponent
	displayname="Application"
	output="true"
	hint="Handle the application.">
<!---	<cfif not cgi.server_port_secure>
        <cflocation url="https://#cgi.server_name##cgi.script_name#?#cgi.query_string#" />
    </cfif>
--->

	<!--- Set up the application. --->
	<cfscript>
		THIS.Name = Hash(getCurrentTemplatePath());
		THIS.ApplicationTimeout = createTimeSpan(0,1,0,0);
		THIS.SessionTimeout = createTimeSpan(0,0,25,0);
		THIS.SessionManagement = true;
		THIS.SetClientCookies = true;
		THIS.ClientManagement = true;
		THIS.ClientStorage = "cookie";
	</cfscript>
	<!--- Define the page request properties. --->
	<cfsetting
		requesttimeout="2400"
		showdebugoutput="true"
		enablecfoutputonly="false"/>

	<cffunction
		name="OnApplicationStart"
		access="public"
		returntype="boolean"
		output="false"
		hint="Fires when the application is first created.">
		
		<cfscript>
			APPLICATION.Datasource = "db_calibration";
			
			// Change vars based on environment
			APPLICATION.rootDir = getDirectoryFromPath(getCurrentTemplatePath());
			
			if(APPLICATION.rootDir CONTAINS "_ldev") {
				env = "dev.";
				APPLICATION.Datasource = "db_calibration_dev";
			}
			if(APPLICATION.rootDir CONTAINS "_dev") {
				env = "dev.";
				APPLICATION.Datasource = "db_calibration_dev";
			}
			if(APPLICATION.rootDir CONTAINS "_uat") {
				env = "uat";
				APPLICATION.Datasource = "db_calibration_dev";		
			}
			if(APPLICATION.rootDir CONTAINS "_www") {
				env = "";
				APPLICATION.Datasource = "db_calibration";
			}
			
			APPLICATION.Datasource = "db_calibration_dev";
			
			APPLICATION.rootURL = "calibration.com";
			APPLICATION.compDir = APPLICATION.rootDir & "_cfcs";
			APPLICATION.ApplicationTimeout = THIS.ApplicationTimeout;
			APPLICATION.SessionTimeout = THIS.SessionTimeout;

			SESSION.loginAttempts = 0;
		</cfscript>
		
		       
		<!--- Return out. --->
		<cfreturn true />
	</cffunction>

	<cffunction
		name="OnRequestStart"
		access="public"
		output="true"
		hint="Fires pre page processing.">

		<cfscript>
			ERR = StructNew();
			ERR.ErrorFound = false;
			ERR.ErrorMessage = "";
			
			LOGIN = StructNew();
			LOGIN.maxAttempts = 25; 
			LOGIN.sessTimeout = CreateTimeSpan(0,0,25,0);
			
			SITE = StructNew();
			SITE.AdminEmail = "Web_Support@hme.com";
			SITE.ITRequest = "ITR@hme.com";
			SITE.MaintUser_IP = "192.168.106.232";
			SITE.IsMaintenance = false; //for putting site into maint mode
			SITE.IsDebug = false; //for putting the site into debug mode
			
			THIS.mappings = StructNew();
		</cfscript>
		
		<cfif (Left(GetFileFromPath(CGI.SCRIPT_NAME),1) IS "_")>
			<cfmail 
				to="Web_Support@hme.com" 
				from="no-reply@hme.com" 
				subject="Sales Portal: Direct Template Browsing Attempt" 
				type="html">
			#DateFormat(now(), "yyyy/mm/dd")# #TimeFormat(now(), "HH:mm:ss")#
			<cfdump var="#CGI#" label="CGI Vars">
			</cfmail>
			<cfabort>
		</cfif>
		
		<cfif StructKeyExists(URL, "reset")>
			<cfif StructClear(SESSION)></cfif>
			<cfset THIS.OnApplicationStart()>
		</cfif>
		
		<!--- Return out. --->
		<cfreturn />
	</cffunction>
	
	<cffunction
		name="OnRequest"
		access="public"
		returntype="void"
		output="true"
		hint="Processes the requested template.">
		<!--- Define arguments. --->
		<cfargument
			name="TargetPage"
			type="string"
			required="true"
			hint="The template being requested." default="#CGI.SCRIPT_NAME#"
			/>
            
        <cfset requestPage = ARGUMENTS.TargetPage>
		<cfset requestPage = ReplaceNoCase(requestPage,"/SalesPortal","./")>
        
		<cfif(APPLICATION.rootDir CONTAINS "_uat") OR (APPLICATION.rootDir CONTAINS "_www")>
			<cfsetting showdebugoutput="no">
        <cfelse>
        	<cfsetting showdebugoutput="yes">
		</cfif>
        
		<cfif CGI.REMOTE_ADDR DOES NOT CONTAIN "10.10." AND SITE.IsMaintenance>
			<cfinclude template="./underconstruction.cfm">
		<cfelse>
			<!--- Include UDF --->
			<cfinclude template="./_udf_global.cfm">
			<!--- Include the requested template. --->
			<cfinclude template="#requestPage#" />
		</cfif>
		 
		<!--- Return out. --->
		<cfreturn />
	</cffunction>
	
	<cffunction name="onError">
		<!---The onError method gets two arguments:
		An exception structure, which is identical to a cfcatch variable.
		The name of the Application.cfc method, if any, in which the error
		happened.--->
		<cfargument name="except" required="yes" />
		<cfargument type="String" name="EventName" required="yes" />
		<!--- Log all errors in an application-specific log file. --->
		<cflog file="#This.Name#" type="error" text="Event Name: #EventName#" >
		<cflog file="#This.Name#" type="error" text="Message: #except.message#">
		<!--- Throw validation errors to ColdFusion for handling. --->
		<cfif Find("coldfusion.filter.FormValidationException", Arguments.Except.StackTrace)>
			<cfthrow object="#except#">
		<cfelse>
			<cfscript>
				ERR = StructNew();
				ERR.Error = except;
			</cfscript>
			<cfinclude template="./security/_tmp_error.cfm">
		</cfif>
	</cffunction>

	<cffunction name="onSessionEnd">
		<cfargument name = "SessionScope" required=true/>
		<cfargument name = "AppScope" required=true/>
		<!---Force Logout--->
		<cfset URL.pg = "Logout">
		<cfinclude template="./index.cfm">	
	</cffunction>

	<cffunction
		name="OnApplicationEnd"
		access="public"
		returntype="void"
		output="false"
		hint="Fires when the application is terminated.">
		
		<!--- Define arguments. --->
		<cfargument
			name="ApplicationScope"
			type="struct"
			required="false"
			default="#StructNew()#"/>
	</cffunction>
</cfcomponent>