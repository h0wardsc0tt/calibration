
<div id="cdb_modal" class="modal fade" role="dialog">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Closed the Day Before</h4>
                </div>
                <div class="modal-body text-center">
                    <div class="row" style="margin:5px 10px 5px 10px;">
                        <div class="col-xs-12">
                            <div class="col-xs-6">
                    			<label for="cdb_start_date" class="control-label">Date Completed From :</label>
                         		<input type="text" maxlength="100" name="cdb_start_date" id="cdb_start_date" class="form-control fai-input-color input-date"/>
                           </div>
                            <div class="col-xs-6">
                    			<label for="cdb_end_date" class="control-label">Date Completed To :</label>
                        		<input type="text" maxlength="100" name="cdb_end_date" id="cdb_end_date" class="form-control fai-input-color input-date"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal" onClick="cdb_modal_continue('cdb');">Continue</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <input type="hidden" id="cdb_req" />
                </div>
            </div>
        </div>
    </div>
</div>

<div id="change_attachchment_name_modal" class="modal fade" role="dialog">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"><span class="glyphicon glyphicon-info-sign"></span> Change Attachment's File Name</h4>
                </div>
                <div class="modal-body text-center">
                    <div class="row" style="margin:5px 10px 5px 10px;">
                        <div class="col-xs-12">
                            <div class="col-xs-12">
                    			<label for="cdb_start_date" class="control-label">File Name :</label>
                         		<input type="text" maxlength="100" name="change-attachment-name" id="attachment_name" class="form-control"/><span id="attachment_ext"></span>
                                <input type="hidden" id="attachment_index"/>
                                <input type="hidden" id="attachment_guid"/>
                                <input type="hidden" id="attachment_prefix"/>
                                <input type="hidden" id="fai_id"/>
                           </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal" onClick="send_attachment_name_change();">Change File Name</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
</div>


<div id="request_fai_modal" class="modal fade" role="dialog">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 id="fai_report_reqested" class="modal-title"></h4>
                </div>
                <div class="modal-body text-center">
                    <div class="row" style="margin:5px 10px 5px 10px;">
                        <div class="col-xs-12">
                            <div class="col-xs-12">
                    			<label for="fai_report_id" class="control-label">Enter FAI Number :</label>
                         		<input type="text" maxlength="10" onKeyPress="if(isEnterKey(event))$('#fai_id_lookup').click();" name="fai_report_id" id="fai_report_id" class="form-control numeric"/>
                                <input type="hidden" id="json_fai_modal"/>
                           </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="fai_id_lookup" type="button" class="btn btn-primary" data-dismiss="modal" onClick="continue_with_report();">Continue</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
</div>


<div id="request_fai_part-number_modal" class="modal fade" role="dialog">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 id="fai_report_reqested" class="modal-title"></h4>
                </div>
                <div class="modal-body text-center">
                    <div class="row" style="margin:5px 10px 5px 10px;">
                        <div class="col-xs-12">
                            <div class="col-xs-12">
                    			<label for="fai_report_id" class="control-label">Enter full or partial supplier name :</label>
                         		<input type="text" maxlength="10" onKeyPress="if(isEnterKey(event))$('#supplier_id_lookup').click();" name="fai_supplier_id" id="fai_supplier_id" class="form-control"/>
                                <input type="hidden" id="json_fai_modal"/>
                           </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="supplier_id_lookup" type="button" class="btn btn-primary" data-dismiss="modal" onClick="fai_dropdown_filter('fpy');">Continue</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
</div>


<div id="admin_modal" class="modal fade" role="dialog" data-backdrop="static">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center">
            <div class="modal-content" style="width:50%;height: auto;">
                <div id="email_header" class="modal-header">
                    <div style="color:#fff; text-align:left;">
                        <i style="font-size:23px;" class="fa  fa-wrench"></i>
                        <div style="display:inline-block; padding-left:10px; font-size:20px;">Admin</div>
                        <div class="col-xs-12 no-pad" style="display:inline-block; font-size:10px;width:75%;float:right;padding-top: 10px;">
							<div class="col-xs-4" onMouseOver="this.style.cursor='pointer';" onclick="admin_menu('admin_manufacturers');">
								Manufacturers
							</div>
							<div class="col-xs-4" onMouseOver="this.style.cursor='pointer';" onclick="admin_menu('admin_intervals');">
								Intervals
							</div>
							<div class="col-xs-4" onMouseOver="this.style.cursor='pointer';" onclick="admin_menu('admin_areas');">
								Areas
							</div>
						</div>

                    </div>
                </div>
                <div class="modal-body text-center" style="padding:0; height:250px;">
					<div id="admin_manufacturers" class="admin-menu" style="display:none;">
						<h1>Manufacturers</h1>
						<div class="col-xs-12 no-pad">
							<div class="col-xs-6">							
										<select class="level1 form-control manufacturer" onchange="adminSelectChanged(this)"  size="10" id="a_manufacturer" name="a_manufacturer">
											<option value="New" >Add New Manufacturer</option>
										</select>							
							</div>
							<div class="col-xs-6">
								<input type="text" maxlength="100" table="manufacturers" previousValue="" style="width: 290px;padding-left:5px;"/>
								<button type="button" class="btn btn-default main" style="display:none;width:75px;" onclick="admin_submit(this)">Add</button>
								<button type="button" class="btn btn-default delete" style="display:none; float:left;" onclick="admin_submit(this)">Delete</button>
								<div class="row" style="float:none;"></div>	
								<div class="errormsg" style="height:50px;color:#337ab7;padding-top:10px;"></div>							
							</div>
							
						</div>
					</div>
					<div id="admin_areas" class="admin-menu" style="display:none;">
						<h1>Areas</h1>
						<div class="col-xs-12 no-pad">
							<div class="col-xs-6">							
										<select class="level1 form-control area" onchange="adminSelectChanged(this)"  size="10" id="a_areas" name="a_areas">
											<option value="New" >Add New Area</option>
										</select>							
							</div>
							<div class="col-xs-6">
								<input type="text" maxlength="100" table="areas" previousValue="" style="width: 290px;padding-left:5px;"/>
								<button type="button" class="btn btn-default main" style="display:none;width:75px;" onclick="admin_submit(this)">Add</button>
								<button type="button" class="btn btn-default delete" style="display:none; float:left;" onclick="admin_submit(this)">Delete</button>
								<div class="row" style="float:none;"></div>	
								<div class="errormsg" style="height:50px;color:#337ab7;padding-top:10px;"></div>							
							</div>
						</div>
					</div>
					
					<div id="admin_intervals" class="admin-menu" style="display:none;">
						<h1>Intervals</h1>
						<div class="col-xs-12 no-pad">
							<div class="col-lg-6">							
										<select class="level1 form-control interval" onchange="adminSelectChanged(this)"  size="10" id="a_intervals" name="a_intervals">
											<option value="New" >Add New Interval</option>
										</select>							
							</div>
							<div class="col-lg-6">
								<input type="text" maxlength="100" table="intervals" previousValue="" style="width: 290px;padding-left:5px;"/>
								<button type="button" class="btn btn-default main" style="display:none;width:75px;" onclick="admin_submit(this)">Add</button>
								<button type="button" class="btn btn-default delete" style="display:none; float:left;" onclick="admin_submit(this)">Delete</button>
								<div class="row" style="float:none;"></div>	
								<div class="errormsg" style="height:50px;color:#337ab7;padding-top:10px;;"></div>							
							</div>
						</div>
					</div>
					
				</div>
               
                <div class="modal-footer">
                     <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="login_modal" class="modal fade" role="dialog" data-backdrop="static">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center">
            <div class="modal-content" style="width:500px;height: auto;">
                <div id="email_header" class="modal-header">
                    <div style="color:#fff; text-align:left;">
                        <i style="font-size:23px;" class="fa  fa-sign-in"></i>
                        <div style="display:inline-block; padding-left:10px; font-size:20px;">Login</div>
                    </div>
                </div>
                <div class="modal-body text-center">
                
                	<iframe id="login_iframe" frameborder="0" style="width:430px; height:340px;" />
                    
                </div>
                <div class="modal-footer">
                    <button id="supplier_id_lookup" type="button" class="btn btn-primary" data-dismiss="modal" onClick="fai_dropdown_filter('fpy');">Continue</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
</div>